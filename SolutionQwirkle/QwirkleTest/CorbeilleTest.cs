﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class CorbeilleTest
    {
        [TestMethod]
        public void AjoutPionCorbeilleTest()
        {
            Joueur joueur = new Joueur(1, "MichelTest");
            Pion[] mainJoueur = new Pion[6];

            mainJoueur[0] = new Pion(1, 1, 1);
            mainJoueur[1] = new Pion(1, 1, 2);
            mainJoueur[2] = new Pion(1, 2, 2);
            mainJoueur[3] = new Pion(1, 2, 3);
            mainJoueur[4] = new Pion(2, 2, 2);
            mainJoueur[5] = new Pion(3, 2, 2);

            Pion[] mainJoueur2 = new Pion[6];

            mainJoueur2[0] = new Pion(1, 1, 1);
            mainJoueur2[1] = new Pion(1, 1, 2);
            mainJoueur2[2] = new Pion(1, 2, 2);
            mainJoueur2[3] = new Pion(1, 2, 3);
            mainJoueur2[4] = new Pion(2, 2, 2);
            mainJoueur2[5] = new Pion(0, 0, 0);

            joueur.MiseAJour_Main(mainJoueur);
            Assert.AreEqual(0, Corbeille.getCorbeilleCount());
            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[5], joueur);
            Assert.AreEqual(mainJoueur2[0].GetId(), joueur.GetMainJoueur()[0].GetId());
            Assert.AreEqual(mainJoueur2[1].GetId(), joueur.GetMainJoueur()[1].GetId());
            Assert.AreEqual(mainJoueur2[2].GetId(), joueur.GetMainJoueur()[2].GetId());
            Assert.AreEqual(mainJoueur2[3].GetId(), joueur.GetMainJoueur()[3].GetId());
            Assert.AreEqual(mainJoueur2[4].GetId(), joueur.GetMainJoueur()[4].GetId());
            Assert.AreEqual(mainJoueur2[5].GetId(), joueur.GetMainJoueur()[5].GetId());
            Assert.AreEqual(1, Corbeille.getCorbeilleCount());
        }

        [TestMethod]
        public void RetourPionMainJoueurTest()
        {
            Joueur joueur = new Joueur(1, "MichelTest");
            Pion[] mainJoueur = new Pion[6];

            mainJoueur[0] = new Pion(1, 1, 1);
            mainJoueur[1] = new Pion(1, 1, 2);
            mainJoueur[2] = new Pion(1, 2, 2);
            mainJoueur[3] = new Pion(1, 2, 3);
            mainJoueur[4] = new Pion(2, 2, 2);
            mainJoueur[5] = new Pion(3, 2, 2);

            Pion[] mainJoueur2 = new Pion[6];

            mainJoueur2[0] = new Pion(1, 1, 1);
            mainJoueur2[1] = new Pion(1, 1, 2);
            mainJoueur2[2] = new Pion(1, 2, 2);
            mainJoueur2[3] = new Pion(1, 2, 3);
            mainJoueur2[4] = new Pion(2, 2, 2);
            mainJoueur2[5] = new Pion(3, 2, 2);

            joueur.MiseAJour_Main(mainJoueur);
            Corbeille.resetCorbeille();
            Assert.AreEqual(0, Corbeille.getCorbeilleCount());
            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[5], joueur);
            Assert.AreEqual(1, Corbeille.getCorbeilleCount());
            Corbeille.RetourPionMainJoueur(joueur);
            Assert.AreEqual(0, Corbeille.getCorbeilleCount());
            Assert.AreEqual(mainJoueur2[0].GetId(), joueur.GetMainJoueur()[0].GetId());
            Assert.AreEqual(mainJoueur2[1].GetId(), joueur.GetMainJoueur()[1].GetId());
            Assert.AreEqual(mainJoueur2[2].GetId(), joueur.GetMainJoueur()[2].GetId());
            Assert.AreEqual(mainJoueur2[3].GetId(), joueur.GetMainJoueur()[3].GetId());
            Assert.AreEqual(mainJoueur2[4].GetId(), joueur.GetMainJoueur()[4].GetId());
            Assert.AreEqual(mainJoueur2[5].GetId(), joueur.GetMainJoueur()[5].GetId());



            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[1], joueur);
            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[4], joueur);
            Assert.AreEqual(2, Corbeille.getCorbeilleCount());
            Corbeille.RetourPionMainJoueur(joueur);
            Assert.AreEqual(mainJoueur2[0].GetId(), joueur.GetMainJoueur()[0].GetId());
            Assert.AreEqual(mainJoueur2[1].GetId(), joueur.GetMainJoueur()[1].GetId());
            Assert.AreEqual(mainJoueur2[2].GetId(), joueur.GetMainJoueur()[2].GetId());
            Assert.AreEqual(mainJoueur2[3].GetId(), joueur.GetMainJoueur()[3].GetId());
            Assert.AreEqual(mainJoueur2[4].GetId(), joueur.GetMainJoueur()[4].GetId());
            Assert.AreEqual(mainJoueur2[5].GetId(), joueur.GetMainJoueur()[5].GetId());
        }

        [TestMethod]
        public void EnvoiePionPiocheTest()
        {
            Joueur joueur = new Joueur(1, "MichelTest");
            Pion[] mainJoueur = new Pion[6];

            mainJoueur[0] = new Pion(1, 1, 1);
            mainJoueur[1] = new Pion(1, 1, 2);
            mainJoueur[2] = new Pion(1, 2, 2);
            mainJoueur[3] = new Pion(1, 2, 3);
            mainJoueur[4] = new Pion(2, 2, 2);
            mainJoueur[5] = new Pion(3, 2, 2);

            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[1], joueur);
            Corbeille.AjoutPionCorbeille(joueur.GetMainJoueur()[4], joueur);
            Assert.AreEqual(2, Corbeille.getCorbeilleCount());
            Corbeille.EnvoiePionPioche();
            Assert.AreEqual(0, Corbeille.getCorbeilleCount());
        }
    }
}
