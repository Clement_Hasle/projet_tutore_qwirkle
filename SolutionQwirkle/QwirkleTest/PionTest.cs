﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class PionTest
    {
        [TestMethod]
        public void GetCouleurTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Pion Pion2 = new Pion(3, 4, 3);
            Pion Pion3 = new Pion(5, 6, 3);
            Assert.AreEqual(1, Pion1.GetCouleur());
            Assert.AreEqual(4, Pion2.GetCouleur());
            Assert.AreEqual(6, Pion3.GetCouleur());
        }

        [TestMethod]
        public void GetFormeTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Pion Pion2 = new Pion(3, 4, 3);
            Pion Pion3 = new Pion(5, 6, 3);
            Assert.AreEqual(1, Pion1.GetForme());
            Assert.AreEqual(3, Pion2.GetForme());
            Assert.AreEqual(5, Pion3.GetForme());
        }

        [TestMethod]
        public void GetTourPlacementTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Assert.AreEqual(0, Pion1.GetTourPlacement());
            
        }

        [TestMethod]
        public void GetIdTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Pion Pion2 = new Pion(3, 4, 3);
            Pion Pion3 = new Pion(5, 6, 3);
            Assert.AreEqual(311, Pion1.GetId());
            Assert.AreEqual(343, Pion2.GetId());
            Assert.AreEqual(365, Pion3.GetId());
        }

        [TestMethod]
        public void SetTourPlacementTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Pion1.SetTourPlacement(5);
            Assert.AreEqual(5, Pion1.GetTourPlacement());
        }

        [TestMethod]
        public void EqualsTest()
        {
            Pion Pion1 = new Pion(1, 1, 3);
            Pion Pion2 = new Pion(1, 1, 3);
            Pion Pion3 = new Pion(1, 1, 4);
            Assert.IsTrue(Pion1==Pion2);
            Assert.IsFalse(Pion1==Pion3);
        }
    }
}