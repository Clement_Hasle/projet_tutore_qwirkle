﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class PartieTest
    {


        [TestMethod]
        public void InitTourTest()
        {
            Partie.InitTour();
            Assert.AreEqual(1,Partie.GetTour());
        }
        
        [TestMethod]
        public void IncrementeTourTest()
        {
            Partie.InitTour();
            Partie.IncrementeTour();
            Partie.IncrementeTour();
            Assert.AreEqual(3, Partie.GetTour());
        }



        [TestMethod]
        public void ConditionFinMainTest()
        {
            Pion[] mainJoueur = new Pion[6];
            Joueur billie = new Joueur(1, "billie");
            mainJoueur[0] = new Pion(0, 0, 0);
            mainJoueur[1] = new Pion(0, 0, 0);
            mainJoueur[2] = new Pion(0, 0, 0);
            mainJoueur[3] = new Pion(0, 0, 0);
            mainJoueur[4] = new Pion(0, 0, 0);
            mainJoueur[5] = new Pion(0, 0, 0);
            billie.MiseAJour_Main(mainJoueur);

            Assert.IsTrue(Partie.ConditionFinMain(billie));

            mainJoueur[5] = new Pion(1, 1, 1);

            billie.MiseAJour_Main(mainJoueur);

            Assert.IsFalse(Partie.ConditionFinMain(billie));
        }

        [TestMethod]
        public void ConditionFinPiocheTest()
        {
            Pioche.InitialisePioche();
            Assert.IsFalse(Partie.ConditionFinPioche());
            Pioche.ClearPioche();
            Assert.IsTrue(Partie.ConditionFinPioche());
        }

        [TestMethod]
        public void PlacementPossibleTest()
        {
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();


            Pion[] mainJoueur = new Pion[6];
            PlacementPion.InitialisationPlacementPion();
            Joueur billie = new Joueur(1, "billie");
            Terrain.InitTerrain();
            mainJoueur[0] = new Pion(1, 1, 1);
            mainJoueur[1] = new Pion(1, 2, 1);
            mainJoueur[2] = new Pion(1, 3, 1);
            mainJoueur[3] = new Pion(1, 4, 1);
            mainJoueur[4] = new Pion(1, 5, 1);
            mainJoueur[5] = new Pion(1, 6, 1);
            billie.MiseAJour_Main(mainJoueur);
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1),7,7);
            Terrain.PlacementPionTerrain(new Pion(1, 2, 1),7,6);
            Terrain.PlacementPionTerrain(new Pion(1, 3, 1),7,5);
            Terrain.PlacementPionTerrain(new Pion(1, 4, 1),7,4);
            Terrain.PlacementPionTerrain(new Pion(1, 5, 1),7,3);
            Terrain.PlacementPionTerrain(new Pion(1, 6, 1),7,2);

            Assert.IsTrue(Partie.PlacementPossible(billie));


            mainJoueur[0] = new Pion(1, 1, 1);
            mainJoueur[1] = new Pion(1, 1, 1);
            mainJoueur[2] = new Pion(1, 1, 1);
            mainJoueur[3] = new Pion(1, 1, 1);
            mainJoueur[4] = new Pion(1, 1, 1);
            mainJoueur[5] = new Pion(1, 1, 1);
            billie.MiseAJour_Main(mainJoueur);
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 7, 7);
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 7, 6);
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 7, 5);
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 7, 4);
            Terrain.PlacementPionTerrain(new Pion(0, 0, 0), 7, 3);
            Terrain.PlacementPionTerrain(new Pion(0, 0, 0), 7, 2);
            Pioche.ClearPioche();
            Assert.IsFalse(Partie.PlacementPossible(billie));
        }
    }
}
