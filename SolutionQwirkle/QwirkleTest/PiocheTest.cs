﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class PiocheTest
    {
        [TestMethod]
        public void InitialisePiocheTest()
        {
            Pioche.InitialisePioche();

            Assert.AreEqual(108, Pioche.GetTaillePioche());
        }

        [TestMethod]
        public void GetTaillePiocheTest()
        {
            Pioche.InitialisePioche();
            Assert.AreEqual(108, Pioche.GetTaillePioche());
        }

        [TestMethod]
        public void GetPionPiocheTest()
        {
            Pioche.InitialisePioche();
            Pion test = Pioche.GetPionPioche();
            Assert.AreEqual(107, Pioche.GetTaillePioche());
            Pion test2 = Pioche.GetPionPioche();
            Pion test3 = Pioche.GetPionPioche();
            Pion test4 = Pioche.GetPionPioche();
            Assert.AreEqual(104, Pioche.GetTaillePioche());
        }
        [TestMethod]
        public void AddPionPiocheTest()
        {
            Pioche.InitialisePioche();
            Pioche.AddPionPioche(new Pion(1, 1, 1));
            Assert.AreEqual(109, Pioche.GetTaillePioche());
            Pioche.AddPionPioche(new Pion(0, 0, 0));
            Assert.AreEqual(109, Pioche.GetTaillePioche());
        }

    }
}