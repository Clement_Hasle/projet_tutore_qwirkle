﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class ComptabilisationTest
    {


        [TestMethod]
        public void ComptageLigneTest()
        {
            int[,] tabTestPlacement = new int[11, 11];

            Terrain.SetNbColonne(10);
            Terrain.SetNbLigne(10);
            tabTestPlacement[0, 4] = 2;
            tabTestPlacement[1, 4] = 2;
            tabTestPlacement[2, 4] = 2;
            tabTestPlacement[3, 4] = 2;
            tabTestPlacement[4, 4] = 2;
            tabTestPlacement[5, 4] = 2;
            tabTestPlacement[1, 0] = 1;
            tabTestPlacement[1, 1] = 1;
            tabTestPlacement[1, 2] = 1;
            tabTestPlacement[1, 3] = 1;
            tabTestPlacement[1, 5] = 1;
            tabTestPlacement[3, 2] = 1;
            tabTestPlacement[3, 3] = 1;
            tabTestPlacement[3, 5] = 1;
            Partie.IncrementeTour();
            
            Assert.AreEqual(4, Comptabilisation.ComptageLigne(tabTestPlacement, 3,4));
            Assert.AreNotEqual(4, Comptabilisation.ComptageLigne(tabTestPlacement, 3,4));
            Assert.AreEqual(12,Comptabilisation.ComptageLigne(tabTestPlacement, 1,4));
            Assert.AreEqual(0,Comptabilisation.ComptageLigne(tabTestPlacement, 1,4));
            Assert.AreNotEqual(12,Comptabilisation.ComptageLigne(tabTestPlacement, 1,4));
            Assert.AreNotEqual(1,Comptabilisation.ComptageLigne(tabTestPlacement, 0,4));

        }

        [TestMethod]
        public void ComptageColonneTest()
        {
            int[,] tabTestPlacement = new int[30, 30];

            tabTestPlacement[0, 4] = 2;
            tabTestPlacement[1, 4] = 2;
            tabTestPlacement[2, 4] = 2;
            tabTestPlacement[3, 4] = 2;
            tabTestPlacement[4, 4] = 2;
            tabTestPlacement[5, 4] = 2;
            tabTestPlacement[1, 0] = 1;
            tabTestPlacement[1, 1] = 1;
            tabTestPlacement[1, 2] = 1;
            tabTestPlacement[1, 3] = 1;
            tabTestPlacement[1, 5] = 1;
            tabTestPlacement[3, 2] = 1;
            tabTestPlacement[3, 3] = 1;
            tabTestPlacement[3, 5] = 1;
            tabTestPlacement[2, 3] = 1;

            Partie.IncrementeTour();
            Assert.AreEqual(12, Comptabilisation.ComptageColonne(tabTestPlacement, 1, 4));
            Assert.AreNotEqual(12, Comptabilisation.ComptageColonne(tabTestPlacement, 1, 4));
            Assert.AreEqual(3, Comptabilisation.ComptageColonne(tabTestPlacement, 3, 3));
            Assert.AreNotEqual(3, Comptabilisation.ComptageColonne(tabTestPlacement, 3, 3));
            Assert.AreEqual(0, Comptabilisation.ComptageColonne(tabTestPlacement, 1, 2));

        }

        [TestMethod]
        public void ComptagePointTest()
        {
            Joueur joueur = new Joueur(1, "test");
            Assert.AreEqual(0, joueur.GetPoint());

            Comptabilisation.MiseAJour_PlacementTour(1, 0);
            Comptabilisation.MiseAJour_PlacementTour(1, 2);
            Comptabilisation.MiseAJour_PlacementTour(1, 1);
            Comptabilisation.MiseAJour_PlacementTour(1, 3);
            Comptabilisation.MiseAJour_PlacementTour(1, 5);
            Comptabilisation.MiseAJour_PlacementTour(3, 2);
            Comptabilisation.MiseAJour_PlacementTour(3, 3);
            Comptabilisation.MiseAJour_PlacementTour(3, 5);
            Comptabilisation.MiseAJour_PlacementTour(2, 3);

            Partie.IncrementeTour();
            Comptabilisation.MiseAJour_PlacementTour(0, 4);
            Comptabilisation.MiseAJour_PlacementTour(1, 4);
            Comptabilisation.MiseAJour_PlacementTour(2, 4);
            Comptabilisation.MiseAJour_PlacementTour(3, 4);
            Comptabilisation.MiseAJour_PlacementTour(4, 4);
            Comptabilisation.MiseAJour_PlacementTour(5, 4);



            Comptabilisation.ComptagePoint(joueur);
            Assert.AreEqual(30, joueur.GetPoint());
        }

        [TestMethod]
        public void InitTabTourTest()
        {
            Partie.InitTour();
            Comptabilisation.MiseAJour_PlacementTour(5, 5);
            Assert.AreEqual(1, Comptabilisation.GetIndiceTabTour(5, 5));
            Comptabilisation.InitTabTour();
            Assert.AreEqual(0, Comptabilisation.GetIndiceTabTour(5, 5));
        }

        [TestMethod]
        public void SupprTabTourTest()
        {
            Partie.InitTour();
            Comptabilisation.MiseAJour_PlacementTour(5, 5);
            Partie.IncrementeTour();
            Comptabilisation.MiseAJour_PlacementTour(5, 6);
            Assert.AreEqual(2, Comptabilisation.GetIndiceTabTour(5, 6));
            Comptabilisation.SupprTabTour();
            Assert.AreEqual(0, Comptabilisation.GetIndiceTabTour(5, 6));
        }

    }
}


