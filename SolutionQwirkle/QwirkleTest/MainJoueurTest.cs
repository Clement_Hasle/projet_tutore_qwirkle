using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;
namespace QwirkleTest
{
    [TestClass]
    public class MainJoueurTest
    {
        Pion[] mainJoueurTest = new Pion[6];
        Pion[] nvMainJoueurTest = new Pion[6];
        
        [TestMethod]
        public void RemplissageMainTest()
        {
            Pioche.InitialisePioche();
            Joueur joueur1 = new Joueur(1, "test");
            mainJoueurTest = joueur1.GetMainJoueur();
            Pion Pion1 = mainJoueurTest[3]; 
            MainJoueur.RetirePionMain(joueur1, 3);
            MainJoueur.RemplissageMain(joueur1);
            nvMainJoueurTest = joueur1.GetMainJoueur();
            // on test le fait que le Pion1 ne fait plus partie de la main du joueur
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[1]);
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[2]);
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[3]);
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[4]);
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[5]);
            Assert.AreNotEqual(Pion1, nvMainJoueurTest[0]); 
            // Ici on test le fait que la main du joueur est remplis, et ne comporte donc aucun Pion qui possède l'id 0.
            Assert.AreNotEqual(0, nvMainJoueurTest[1].GetId());
            Assert.AreNotEqual(0, nvMainJoueurTest[2].GetId());
            Assert.AreNotEqual(0, nvMainJoueurTest[3].GetId());
            Assert.AreNotEqual(0, nvMainJoueurTest[0].GetId());
            Assert.AreNotEqual(0, nvMainJoueurTest[5].GetId()); 
            Assert.AreNotEqual(0, nvMainJoueurTest[4].GetId());
            // Ici on test le fait que la nouvelle main est egale a l'ancienne, sauf l'index 3 qui est le pion qui a été échanger
            Assert.IsTrue(mainJoueurTest[0] == nvMainJoueurTest[0]);
            Assert.IsTrue(mainJoueurTest[1] == nvMainJoueurTest[1]);
            Assert.IsTrue(mainJoueurTest[2] == nvMainJoueurTest[2]);
            Assert.IsFalse(Pion1 == nvMainJoueurTest[3]);
            Assert.IsTrue(mainJoueurTest[4] == nvMainJoueurTest[4]);
            Assert.IsTrue(mainJoueurTest[5] == nvMainJoueurTest[5]);
        }
            
        [TestMethod]
        public void RetirePionMainTest()
        {

            Joueur joueur = new Joueur(1, "test");
            
            MainJoueur.RetirePionMain(joueur, 2);
            mainJoueurTest = joueur.GetMainJoueur();

            Assert.AreEqual(0, mainJoueurTest[2].GetId());
            // on test si le pion a l'index 2 est bien un pion a ID 0.
        }                                                        

    }
}