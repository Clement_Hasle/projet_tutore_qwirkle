﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class JoueurTest
    {


        Pion[] mainJoueurTest = new Pion[6];

        [TestMethod]
        public void GetNomTest()
        {
            Joueur joueur1 = new Joueur(1, "Remi");
            Assert.AreEqual("Remi", joueur1.GetNom());

        }

        [TestMethod]
        public void GetPointTest()
        {
            Joueur joueur1 = new Joueur(1, "Remi");
            Assert.AreEqual(0, joueur1.GetPoint());

        }

        [TestMethod]
        public void GetIdTest()
        {
            Joueur joueur1 = new Joueur(1, "Remi");
            Assert.AreEqual(1, joueur1.GetId());

        }

        [TestMethod]
        public void MiseAJour_PointTest()
        {
            Joueur joueur1 = new Joueur(1, "Remi");
            joueur1.MiseAJour_Point(100);
            Assert.AreEqual(100, joueur1.GetPoint());

        }

        [TestMethod]
        public void SetNomTest()
        {
            Joueur joueur1 = new Joueur(1, "Remi");
            joueur1.SetNom("Will");
            Assert.AreEqual("Will", joueur1.GetNom());

        }

        [TestMethod]
        public void GetMainJoueurTest()
        {

            for (int i = 0; i < 6; i++)
            {
                mainJoueurTest[i] = new Pion(0, 0, 0);
            }

            Joueur joueur1 = new Joueur(1, "Remi");


            for (int i = 0; i < 6; i++)
            {
                Assert.IsTrue(mainJoueurTest[i] == joueur1.GetMainJoueur()[i]);
            }


        }

        [TestMethod]
        public void MiseAJour_MainTest()
        { 
            Joueur joueur1 = new Joueur(1, "Remi");

            for (int i = 0; i < 6; i++)
            {
                mainJoueurTest[i] = new Pion(1, 0, 0);
            }
            joueur1.MiseAJour_Main(mainJoueurTest);

            for (int i = 0; i < 6; i++)
            {
                Assert.IsTrue(mainJoueurTest[i] == joueur1.GetMainJoueur()[i]);
            }

        }
    }
}

