﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;
using System.Collections.Generic;

namespace QwirkleTest
{
    [TestClass]
    public class PlacementPionTest
    {
        //Nouveau Test OK

        [TestMethod]
        public void VerificationPlacementTest()
        {
            Terrain.InitTerrain();
            Partie.InitTour();
            PlacementPion.InitialisationPlacementPion();
            //Verification de L'ajout du 1er Pion
            Assert.IsTrue(PlacementPion.VerificationPlacement(0, 0, new Pion(2, 2, 1)));
            Terrain.PlacementPionTerrain(new Pion(2, 2, 1), 0, 0);
            Terrain.VerifPionLimite();
            //Verification du Second Pion
            Assert.IsFalse(PlacementPion.VerificationPlacement(1, 1, new Pion(2, 1, 1)));
            Assert.IsFalse(PlacementPion.VerificationPlacement(2, 2, new Pion(1, 1, 1)));
            Assert.IsFalse(PlacementPion.VerificationPlacement(1, 2, new Pion(1, 1, 1)));
            Assert.IsTrue(PlacementPion.VerificationPlacement(1, 2, new Pion(1, 2, 1)));
            Terrain.PlacementPionTerrain(new Pion(1, 2, 1), 1, 2);
            Terrain.VerifPionLimite();
            //Verification Placement dans l'axe du Troisième
            Assert.IsFalse(PlacementPion.VerificationPlacement(1, 2, new Pion(3, 3, 1)));
            Assert.IsFalse(PlacementPion.VerificationPlacement(2, 3, new Pion(2, 3, 1)));
            Assert.IsFalse(PlacementPion.VerificationPlacement(1, 3, new Pion(2, 3, 1)));
            Assert.IsTrue(PlacementPion.VerificationPlacement(1, 3, new Pion(3, 2, 1)));
        }
    }
}