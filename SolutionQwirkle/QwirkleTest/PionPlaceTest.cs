﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class PionPlaceTest
    {
        [TestMethod]
        public void PionPlacetest()
        {
            Pion pion = new Pion(1, 1, 1);
            PionPlace pionPlace = new PionPlace(pion, 1, 2);
            Assert.AreEqual(111, pionPlace.GetPion().GetId());
            Assert.AreEqual(1, pionPlace.GetX());
            Assert.AreEqual(2, pionPlace.GetY());
        }

        [TestMethod]
        public void SetXTest()
        {
            Pion pion = new Pion(1, 1, 1);
            PionPlace pionPlace = new PionPlace(pion, 1, 2);
            pionPlace.SetX(5);
            Assert.AreEqual(5, pionPlace.GetX());
        }

        [TestMethod]
        public void SetYTest()
        {
            Pion pion = new Pion(1, 1, 1);
            PionPlace pionPlace = new PionPlace(pion, 1, 2);
            pionPlace.SetY(5);
            Assert.AreEqual(5, pionPlace.GetY());
        }

    }
}