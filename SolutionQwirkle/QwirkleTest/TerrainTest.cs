﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleConsole;

namespace QwirkleTest
{
    [TestClass]
    public class TerrainTest
    {
        [TestMethod]
        public void InitTerrainTest()
        {
            Terrain.InitTerrain();
            Assert.AreEqual(0, Terrain.GetPionTerrain(0, 0).GetId());
        }



        [TestMethod]
        public void PlacementPionTerrainTest()
        {
            Terrain.InitTerrain();
            Assert.AreEqual(0, Terrain.GetPionTerrain(0, 0).GetId());
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Assert.AreEqual(111, Terrain.GetPionTerrain(0, 0).GetId());
        }

        [TestMethod]
        public void RetirePionTerrainTest()
        {
            Terrain.InitTerrain();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.RetirePionTerrain(0, 0);
            Assert.AreEqual(0, Terrain.GetPionTerrain(0, 0).GetId());
        }

        [TestMethod]
        public void VerifPionLimiteTest()
        {
            Terrain.InitTerrain();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Assert.AreEqual(5, Terrain.VerifPionLimite());
        }

        [TestMethod]
        public void ElargissementTerrainTest()
        {
            Terrain.InitTerrain();
            Terrain.PlacementPionTerrain(new Pion(1, 1, 1), 0, 0);
            Terrain.VerifPionLimite();
            Assert.AreEqual(0, Terrain.GetPionTerrain(0, 0).GetId());
            Assert.AreEqual(111, Terrain.GetPionTerrain(1, 1).GetId());
            Assert.AreEqual(0, Terrain.GetPionTerrain(2, 1).GetId());
            Assert.AreEqual(0, Terrain.GetPionTerrain(1, 2).GetId());
        }
            
    }
}
