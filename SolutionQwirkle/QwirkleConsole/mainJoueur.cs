using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class MainJoueur
    {
        //Ce n'est pas un objet. Ne l'instanciez pas.

        public static void RemplissageMain(Joueur joueur)
        {
            Pion[] mainJoueur = new Pion[6];
            Pion[] mainJoueurOrdonner= new Pion[6]; // représente la main du joueur dans l'ordre ( pion 0 ) quand la main est vide.
            int pionManquant =0; // définie le nombre de pion a ajouter pour que la main du joueur soit complète
            mainJoueur = joueur.GetMainJoueur();
            int indexPion = 0;
            for (int i=0;i<6;i++) // calcul du nombre de pion dans la main du joueur
            {
                if (mainJoueur[i].GetId()== 0)
                {
                    pionManquant++;
                } else
                {
                    mainJoueurOrdonner[indexPion]= mainJoueur[i];
                    indexPion++;
                }
            }

            while (pionManquant-- > 0)
            {
                mainJoueurOrdonner[indexPion++] = Pioche.GetPionPioche();
            }

            joueur.MiseAJour_Main(mainJoueurOrdonner);

        }

        public static void RetirePionMain(Joueur joueur, int index)
        {
            Pion[] mainJoueur = new Pion[6];
            mainJoueur = joueur.GetMainJoueur(); // On récupère la main du joueur
            mainJoueur[index] = new Pion(0, 0, 0); 
        }
    }
}