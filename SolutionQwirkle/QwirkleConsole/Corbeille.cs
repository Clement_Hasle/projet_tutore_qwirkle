﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Corbeille
    {
        private static List<Pion> corbeille = new List<Pion>(); //Les pions que le joueur veut changer.
        private static Pion[] mainJoueur = new Pion[6];

        public static void AjoutPionCorbeille(Pion pion, Joueur joueur)
        {
            corbeille.Add(pion);                    //On ajoute le pion à la corbeille
            mainJoueur = joueur.GetMainJoueur();    //On fusionne les deux tableaux (Le tableau du Joueur donne sa référence à mainJoueur
            for(int indiceMainJoueur = 0; indiceMainJoueur < 6; indiceMainJoueur++)   //On cherche le pion a supprimer
            { 
                if(mainJoueur[indiceMainJoueur].GetId() == pion.GetId())   //Quand le pion dans la main du joueur correspond au pion dans la corbeille
                {
                    mainJoueur[indiceMainJoueur] = new Pion(0,0,0);                   //On retire le pion
                }
            }
        }

        public static void RetourPionMainJoueur(Joueur joueur)  // Si le joueur annule son jetage de pion
        {
            int indicePoubelle = 0;
            for(int indiceMainJoueur=0; indiceMainJoueur < 6; indiceMainJoueur++)
            {
                if(mainJoueur[indiceMainJoueur].GetId()==0)
                {
                    mainJoueur[indiceMainJoueur] = corbeille[indicePoubelle++];
                }
            }
            corbeille.Clear();
        }

        public static void EnvoiePionPioche()
        {
            foreach (Pion indice in corbeille)
                Pioche.AddPionPioche(indice);
            corbeille.Clear();
        }

        public static int getCorbeilleCount()
        {
            return corbeille.Count;
        }

        public static void resetCorbeille()
        {
            corbeille.Clear();
        }
    }
}
