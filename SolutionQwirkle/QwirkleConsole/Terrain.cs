﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Terrain
    {
        //Ce n'est pas un objet. Ne l'instanciez pas.
        public static Pion[,] terrain;
        private static int nbColonne = 1;
        private static int nbLigne = 1;
        private static int nbAjoute = 1;
        private static bool premsPion;
        private static int xCentral = 0;
        private static int yCentral = 0;

        public static void InitTerrain()
        {
            premsPion = true;
            nbColonne = 1;
            nbLigne = 1;
            terrain = new Pion[nbColonne, nbLigne];
            for (int x = 0; x < nbColonne; x++)
                for (int y = 0; y < nbLigne; y++)
                    terrain[x,y] = new Pion(0, 0, 0);
        }

        public static void InitCentre()
        {
            xCentral = 0;
            yCentral = 0;
        }

        public static void PlacementPionTerrain(Pion pion, int x, int y)
        {
            terrain[x, y] = pion;
        }

        public static Pion GetPionTerrain(int x, int y)
        {
            return terrain[x,y];
        }

        public static void RetirePionTerrain(int x, int y)
        {
            terrain[x, y] = new Pion(0, 0, 0);
        }

        public static Pion[,] GetTerrain()
        {
            return terrain;
        }

        public static int GetNbColonne()
        {
            return nbColonne;
        }

        public static int GetNbLigne()
        {
            return nbLigne;
        }

        public static void SetNbColonne(int x)
        {
            nbColonne = x;
        }

        public static void SetNbLigne(int y)
        {
            nbLigne = y;
        }

        public static int GetNbAjoute()
        {
            return nbAjoute;
        }

        public static int GetXCentral()
        {
            return xCentral;
        }

        public static int GetYCentral()
        {
            return yCentral;
        }

        public static int VerifPionLimite()             // renvoie une liste de chiffres entre 0 et 4     0: pas de pion - 1: pion en haut - 2: pion à droite - 3: en bas - 4: à gauche - 5: Les 8 pions autour.
        {
            int orientation = 0;
            if (!premsPion)
            {
                for (int x = 0; x < nbColonne; x++)
                    if (terrain[x, 0].GetId() != 0)
                        orientation = 1;
                for (int x = 0; x < nbColonne; x++)
                    if (terrain[x, nbLigne - 1].GetId() != 0)
                        orientation = 3;
                for (int y = 0; y < nbLigne; y++)
                    if (terrain[nbColonne - 1, y].GetId() != 0)
                        orientation = 2;
                for (int y = 0; y < nbLigne; y++)
                    if (terrain[0, y].GetId() != 0)
                        orientation = 4;
            }
            else
            {
                orientation = 5;
                premsPion = false;
            }

            ElargissementTerrain(orientation);
            Comptabilisation.TabRedimensionnement(orientation);
            return orientation;
        }


        public static void ElargissementTerrain(int orientation)
        {
            switch (orientation)
            {
                case 1:
                    PlacementPion.ChangeCoordonnéeXYPremierPion(0, 1);
                    ajoutLigneHaut();
                    break;
                case 2:
                    ajoutColonneDroite();
                    break;
                case 3:
                    ajoutLigneBas();
                    break;
                case 4:
                    PlacementPion.ChangeCoordonnéeXYPremierPion(1, 0);
                    ajoutColonneGauche();
                    break;
                case 5:
                    PlacementPion.ChangeCoordonnéeXYPremierPion(1, 1);
                    ajoutCarree();
                    break;
                default:
                    break;
            }
        }

        public static void ajoutColonneDroite()
        {
            nbColonne = nbColonne + nbAjoute;                      // on ajoute trois colonne
            Pion[,] newTerrain = new Pion[nbColonne, nbLigne];

            for (int x = 0; x < nbColonne-nbAjoute; x++)           // On copie le tableau de base.
                for (int y = 0; y < nbLigne; y++)
                {
                     newTerrain[x, y] = terrain[x, y];
                }

            for (int x = nbColonne-nbAjoute; x < nbColonne; x++)  // On remplie les cases qui se sont ajoutées
                for (int y = 0; y < nbLigne; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }
            terrain = new Pion[nbColonne, nbLigne];         // On redimensionne le tableau
            terrain = newTerrain;                           // On copie le nouveau tableau dans l'ancien
        }

        public static void ajoutColonneGauche()
        {
            xCentral = xCentral + 1;
            nbColonne = nbColonne + nbAjoute;
            Pion[,] newTerrain = new Pion[nbColonne, nbLigne];

            for (int x = 0; x < nbColonne-nbAjoute; x++)       
                for (int y = 0; y < nbLigne; y++)
                {
                    newTerrain[x + nbAjoute, y] = terrain[x, y];       //Ici, on ajoute des colonnes à gauche donc il faut décaler les pions
                }

            for (int x = 0; x < nbAjoute; x++)
                for (int y = 0; y < nbLigne; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;
        }

        public static void ajoutLigneHaut()
        {
            yCentral = yCentral + 1;
            nbLigne = nbLigne + nbAjoute;
            Pion[,] newTerrain = new Pion[nbColonne, nbLigne];

            for (int x = 0; x < nbColonne; x++)
                for (int y = 0; y < nbLigne - nbAjoute; y++)
                {
                     newTerrain[x, y+nbAjoute] = terrain[x, y];
                }

            for (int x = 0; x < nbColonne; x++)
                for (int y = 0; y < nbAjoute; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;
        }

        public static void ajoutLigneBas()
        {
            nbLigne = nbLigne + nbAjoute;
            Pion[,] newTerrain = new Pion[nbColonne, nbLigne];

            for(int x = 0; x < nbColonne; x++)
                for(int y = 0; y < nbLigne-nbAjoute; y++)
                {
                     newTerrain[x, y] = terrain[x, y];
                }

            for (int x = 0; x < nbColonne; x++)
                for (int y = nbLigne-nbAjoute; y < nbLigne; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;
        }

        public static void ajoutCarree()
        {
            xCentral = xCentral + 1;
            yCentral = yCentral + 1;
            nbLigne = nbLigne + nbAjoute;
            Pion[,] newTerrain = new Pion[nbColonne, nbLigne];
            newTerrain[0, 0] = new Pion(0, 0, 0);
            newTerrain[0, nbLigne-1] = terrain[0, 0];
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;

            nbColonne = nbColonne + nbAjoute;
            newTerrain = new Pion[nbColonne, nbLigne];
            newTerrain[0, 0] = newTerrain[0, nbLigne-1] = newTerrain[nbColonne-1, 0] = new Pion(0, 0, 0);
            newTerrain[nbColonne-1, nbLigne-1] = terrain[0, nbLigne-1];
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;

            nbLigne = nbLigne + nbAjoute;
            newTerrain = new Pion[nbColonne, nbLigne];
            for(int x=0; x<nbColonne; x++)
                for(int y=0; y<nbLigne-nbAjoute; y++)
                {
                    newTerrain[x, y] = terrain[x, y];
                }

            for (int x = 0; x < nbColonne; x++)
                for (int y = nbLigne - nbAjoute; y < nbLigne; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }

            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;

            nbColonne = nbColonne + nbAjoute;                      
            newTerrain = new Pion[nbColonne, nbLigne];

            for (int x = 0; x < nbColonne - nbAjoute; x++)          
                for (int y = 0; y < nbLigne; y++)
                {
                    newTerrain[x, y] = terrain[x, y];
                }

            for (int x = nbColonne - nbAjoute; x < nbColonne; x++) 
                for (int y = 0; y < nbLigne; y++)
                {
                    newTerrain[x, y] = new Pion(0, 0, 0);
                }
            terrain = new Pion[nbColonne, nbLigne];
            terrain = newTerrain;
        }
    }
}
