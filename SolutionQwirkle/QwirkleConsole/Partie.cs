using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Partie
    {   //Premier Joueur a Jouer tour 0 suivant tour 1 (pour verife placement ==> sinon modification a demander a william) ou sinon faire tour global(tout le monde incrementer le tour de 1)
        private static int tour = 1;
        private static Pion[] mainJoueur = new Pion[6];
        static List<Joueur> joueurOrdre = new List<Joueur>() ;  // crée une liste intermédiaire
        public static void IncrementeTour()
        {
            tour++;
        }

        public static void InitTour()
        {
            tour = 1;
        }

        public static bool ConditionFinMain(Joueur joueur)
        {
            mainJoueur = joueur.GetMainJoueur();
            for (int i = 0; i < 6; i++)
            {
                if (mainJoueur[i].GetId() != 0)
                    return false;
            }
            joueur.MiseAJour_Point(joueur.GetPoint()+2);

            return true;
        }

        public static bool ConditionFinPioche()
        {
            if (Pioche.GetTaillePioche() <= 0)
                return true;
            return false;
        }

        public static List<Joueur> FinPartie(List<Joueur> joueur)
        {
            int nbJoueur = joueur.Count;
            for (int i = 0; i < nbJoueur ; i++)
            {
                if (i >= 1 && joueur[i].GetPoint() > joueur[i - 1].GetPoint())
                {
                    joueurOrdre.Add(joueur[i-1]);
                    joueur.RemoveAt(i-1);
                    joueur.Add(joueurOrdre[0]);
                    joueurOrdre.RemoveAt(0);
                    

                    if (i==1)
                    {
                        i--;
                    }else if (i==2)
                    {
                        i = i - 2;
                    }
                    else if (i == 3)
                    {
                        i = i - 3;
                    }
                }
            }
        return joueur;
        }

        public static int GetTour() //dire a william la modif pour les paramètre ==> Test a faire et code (si modif) (important)
        {
            return tour;
        }

        public static bool PlacementPossible(Joueur joueur)
        {
            PlacementPion.InitialisationPlacementPion();

            Pion[] mainJoueur = new Pion[6];
            mainJoueur = joueur.GetMainJoueur();
            foreach (Pion pion in mainJoueur)
            {
                for (int x = 0; x < Terrain.GetNbColonne(); x++)
                {
                    for (int y = 0; y < Terrain.GetNbLigne(); y++)
                    {
                        if (PlacementPion.VerificationPlacement(x, y, pion))
                            return true;
                    }
                }
            }

            return false;
        }
    }
}