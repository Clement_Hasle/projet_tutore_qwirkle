﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Joueur
    {
        private int point;
        private string nom;
        private int id;
        private Pion[] mainJoueur = new Pion[6];

        public Joueur(int id, string nom)
        {
            this.id = id;
            this.nom = nom;
            point = 0;

            for (int i = 0; i < 6; i++)
            {
                this.mainJoueur[i] = new Pion(0, 0, 0);
            }

        }

        public string GetNom()
        {
            return this.nom;
        }

        public int GetPoint()
        {
            return this.point;
        }

        public int GetId()
        {
            return this.id;
        }

        public Pion[] GetMainJoueur()
        {
            return this.mainJoueur;
        }

        public void SetNom(string nvNom)
        {
            this.nom = nvNom;
        }

        public void MiseAJour_Point(int nvPoint)
        {
            this.point = nvPoint;
        }

        public void MiseAJour_Main(Pion[] nvMain)
        {
            for (int compteur = 0; compteur < 6; compteur++)
            {
                this.mainJoueur[compteur] = nvMain[compteur];
            }
        }
    }
}
