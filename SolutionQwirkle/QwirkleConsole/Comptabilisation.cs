﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Comptabilisation
    {

        public static int[,] tableauModifie = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];                                         //Ce tableau copiera placementTour et me servira à remplacer les pions déjà compté dans le score
        public static int[,] placementTour = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];                                         //Ce tableau copiera placementTour et me servira à remplacer les pions déjà compté dans le score

        public static void ComptagePoint(Joueur joueur)
        {
            //throw new NotImplementedException();
            int pointAjoute = 0;

            for (int x = 0; x < Terrain.GetNbColonne(); x++)                                    // On copie le tableau placementTour dans TableauModifie. Pas de tableauModifie = placementTour !!!
            { 
                for (int y = 0; y < Terrain.GetNbLigne(); y++)                                            
                {
                    tableauModifie[x, y] = placementTour[x, y];
                    
                }
            }

            for (int x = 0; x < Terrain.GetNbColonne(); x++)                                                // On parcourt le tableau en x jusqu'à trouver un pion qui a été posé dans le tour actuel
            {
                for (int y = 0; y < Terrain.GetNbLigne(); y++)                                            // On parcourt le tableau en y jusqu'à trouver un pion qui a été posé dans le tour actuel
                {
                    if (placementTour[x, y] == Partie.GetTour())                                    // Quand le pion a été trouvé, on accède à la condition
                    {
                        pointAjoute = pointAjoute + ComptageLigne(tableauModifie, x, y) + ComptageColonne(tableauModifie, x, y);         // On va compter le nombre de point à ajouter
                    }
                }
            }

            if (Partie.GetTour() == 1 && pointAjoute == 0)
                pointAjoute = 1;
            joueur.MiseAJour_Point(joueur.GetPoint() + pointAjoute);                                                     // On ajoute les points aux points que le joueur possède déjà
        }

        public static int ComptageColonne(int[,] tableauModifie, int x, int y)
        {
            int totaleColonne = 1;                                                                    // On démarre à un car le pion detecté
            tableauModifie[x, y] = 0;                                                              // On efface la présence du pion de départ de cette fonction pour éviter de le compter en mille fois.
            int indiceLigne = x - 1;

            while (indiceLigne >= 0 && tableauModifie[indiceLigne, y]!=0)             // On check tous les pions à gauche de celui qui a été détecté
            {
                if (tableauModifie[indiceLigne, y] <= Partie.GetTour() && tableauModifie[indiceLigne, y] > 0)   // On vérifie que les pions existent
                {
                    totaleColonne++;                                                             // On ajoute un point à chaque fois qu'on trouve un pion adjacent
                    tableauModifie[indiceLigne, y] = 0;                                        // On suppprime ce pion du tableau de copie pour ne pas le compter deux fois.
                }

                --indiceLigne;
            }

            indiceLigne = x + 1;

            while (indiceLigne <= Terrain.GetNbColonne()-1 && tableauModifie[indiceLigne, y] != 0)             // On check tous les pions à droite de celui qui a été détecté
            {
                if (tableauModifie[indiceLigne, y] <= Partie.GetTour() && tableauModifie[indiceLigne, y] > 0)   // On vérifie que les pions existent
                {
                    totaleColonne++;                                                             // On ajoute un point à chaque fois qu'on trouve un pion adjacent
                    tableauModifie[indiceLigne, y] = 0;                                        // On suppprime ce pion du tableau de copie pour ne pas le compter deux fois.
                }

                ++indiceLigne;
            }

            if (totaleColonne == 6)                  // Si on a 6 points, ça veut dire qu'on a fait un Qwirkle donc on double
            {
                totaleColonne = 12;
            }

            if (totaleColonne == 1)                  // Si on ne trouve pas de pion adjacent horizontalement, on annule point.
            {
                totaleColonne = 0;
            }

            return totaleColonne;
        }

        public static int ComptageLigne(int[,] tableauModifie, int x, int y)
        {

            int totaleLigne = 1;
            int indiceColonne = y - 1;

            while (indiceColonne >= 0 && tableauModifie[x, indiceColonne] != 0)                     // On check tous les pions en dessous de celui qui a été détecté
            {
                if (tableauModifie[x, indiceColonne] <= Partie.GetTour() && tableauModifie[x, indiceColonne] > 0)       // On vérifie que les pions sont existent
                {
                    totaleLigne++;                                                                // On ajoute un point à chaque fois qu'on trouve un pion adjacent
                    tableauModifie[x, indiceColonne] = 0;                                           // On suppprime ce pion du tableau de copie pour ne pas le compter deux fois.
                }
                indiceColonne--;
            }

            indiceColonne = y + 1;
            while (indiceColonne <= Terrain.GetNbLigne()-1 && tableauModifie[x, indiceColonne] != 0)                     // On check tous les pions en dessous de celui qui a été détecté
            {
                if (tableauModifie[x, indiceColonne] <= Partie.GetTour() && tableauModifie[x, indiceColonne] > 0)       // On vérifie que les pions sont existent
                {
                    totaleLigne++;                                                                // On ajoute un point à chaque fois qu'on trouve un pion adjacent
                    tableauModifie[x, indiceColonne] = 0;                                           // On suppprime ce pion du tableau de copie pour ne pas le compter deux fois.
                }

                indiceColonne++;
            }

            if (totaleLigne == 6)
            {
                totaleLigne = 12;
            }

            if (totaleLigne == 1)
            {
                totaleLigne = 0;
            }

            return totaleLigne;
        }

        public static void MiseAJour_PlacementTour(int x, int y)
        {
            placementTour[x, y] = Partie.GetTour();
        }

        public static void InitTabTour()
        {
            for (int indice1 = 0; indice1 < Terrain.GetNbColonne(); indice1++)
                for (int indice2 = 0; indice2 < Terrain.GetNbLigne(); indice2++)
                    placementTour[indice1, indice2] = 0;
        }

        public static int GetIndiceTabTour(int x, int y)
        {
            return placementTour[x, y];
        }

        public static void SupprTabTour()
        {
            for (int indice1 = 0; indice1 < Terrain.GetNbColonne(); indice1++)
                for (int indice2 = 0; indice2 < Terrain.GetNbLigne(); indice2++)
                    if(placementTour[indice1,indice2]==Partie.GetTour())
                        placementTour[indice1, indice2] = 0;
        }

        public static void ResetTourTabTour()
        {
            for (int indice1 = 0; indice1 < Terrain.GetNbColonne(); indice1++)
                for (int indice2 = 0; indice2 < Terrain.GetNbLigne(); indice2++)
                    if(placementTour[indice1,indice2] == Partie.GetTour())
                         placementTour[indice1, indice2] = 0;
        }

        public static void TabRedimensionnement(int orientation)
        {
            tableauModifie = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];
                switch (orientation)
                {
                    case 1:
                        ajoutLigneHaut();
                        break;
                    case 2:
                        ajoutColonneDroite();
                        break;
                    case 3:
                        ajoutLigneBas();
                        break;
                    case 4:
                        ajoutColonneGauche();
                        break;
                    case 5:
                        ajoutCarree();
                        break;
                    default:
                        break;
                }
        }

        public static void ajoutColonneDroite()
        {
           int[,] newTab = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()]; // On creer un tab intermédiairre

            for (int x = 0; x < (Terrain.GetNbColonne() - Terrain.GetNbAjoute()); x++)           // On copie le tableau de base.
                for (int y = 0; y < Terrain.GetNbLigne(); y++)
                {
                    newTab[x, y] = placementTour[x, y];
                }

            for (int x = Terrain.GetNbColonne() - Terrain.GetNbAjoute(); x < Terrain.GetNbColonne(); x++)  // On remplie les cases qui se sont ajoutées
                for (int y = 0; y < Terrain.GetNbLigne(); y++)
                {
                    newTab[x, y] = 0;
                }
            placementTour = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];         // On redimensionne le tableau
            placementTour = newTab;                           // On copie le nouveau tableau dans l'ancien
        }

        public static void ajoutColonneGauche()
        {
            int[,] newTab = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];

            for (int x = 0; x < Terrain.GetNbColonne() - Terrain.GetNbAjoute(); x++)
                for (int y = 0; y < Terrain.GetNbLigne(); y++)
                {
                    newTab[x + Terrain.GetNbAjoute(), y] = placementTour[x, y];       //Ici, on ajoute des colonnes à gauche donc il faut décaler les pions
                }

            for (int x = 0; x < Terrain.GetNbAjoute(); x++)
                for (int y = 0; y < Terrain.GetNbLigne(); y++)
                {
                    newTab[x, y] = 0;
                }
            placementTour = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];
            placementTour = newTab;
        }

        public static void ajoutLigneHaut()
        {
            int[,] newTab = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];

            for (int x = 0; x < Terrain.GetNbColonne(); x++)
                for (int y = 0; y < Terrain.GetNbLigne() - Terrain.GetNbAjoute(); y++)
                {
                    newTab[x, y + Terrain.GetNbAjoute()] = placementTour[x, y];
                }

            for (int x = 0; x < Terrain.GetNbColonne(); x++)
                for (int y = 0; y < Terrain.GetNbAjoute(); y++)
                {
                    newTab[x, y] = 0;
                }
            placementTour = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];
            placementTour = newTab;
        }

        public static void ajoutLigneBas()
        {
            int[,] newTab = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];

            for (int x = 0; x < Terrain.GetNbColonne(); x++)
                for (int y = 0; y < Terrain.GetNbLigne() - Terrain.GetNbAjoute(); y++)
                {
                    newTab[x, y] = placementTour[x, y];
                }

            for (int x = 0; x < Terrain.GetNbColonne(); x++)
                for (int y = Terrain.GetNbLigne() - Terrain.GetNbAjoute(); y < Terrain.GetNbLigne(); y++)
                {
                    newTab[x, y] = 0;
                }
            placementTour = new int[Terrain.GetNbColonne(), Terrain.GetNbLigne()];
            placementTour = newTab;
        }


        public static void ajoutCarree()
        {
            int nbLigne = Terrain.GetNbLigne(), nbColonne = Terrain.GetNbColonne();

            int[,] newTab = new int[nbColonne, nbLigne];

            for (int x = 0; x < nbLigne; x++)
                for (int y = 0; y < nbColonne; y++)
                    newTab[x, y] = 0;

            newTab[1, 1] = placementTour[0, 0];
            placementTour = new int[nbColonne, nbLigne];
            placementTour = newTab;
        }
    }
}