﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Pion
    {
        private int couleur;
        private int forme;
        private int tourPlacement;
        private int id;
        
        public Pion(int forme, int couleur, int exemplaire)
        {
            this.couleur = couleur;
            this.forme = forme;
            tourPlacement = 0;
            id = forme + 10 * couleur + 100 * exemplaire;
        }

        public int GetCouleur()
        {
            return this.couleur;
        }

        public int GetForme()
        {
            return this.forme;
        }

        public int GetTourPlacement()
        {
            return this.tourPlacement;
        }

        public int GetId()
        {
            return id;
        }

        public void SetTourPlacement(int tour)
        {
            this.tourPlacement = tour;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.couleur.GetHashCode();
            hash ^= this.forme.GetHashCode();
            hash ^= this.id.GetHashCode();
            return hash;
        }

        public static bool operator ==(Pion pion1, Pion pion2)
        {
            return pion1.couleur == pion2.couleur && pion1.forme == pion2.forme && pion1.id == pion2.id;
        }

        public static bool operator !=(Pion pion1, Pion pion2)
        {
            return !(pion1 == pion2);
        }

        public override bool Equals(object obj)
        {
            return obj is Pion && this == (Pion)obj;
        }



    }
}

