﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class PionPlace
    {
        private Pion pion;
        private int x;
        private int y;

        public PionPlace(Pion pion, int x, int y)
        {
            this.pion = pion;
            this.x = x;
            this.y = y;
        }

        public Pion GetPion()
        {
            return pion;
        }

        public int GetX()
        {
            return x;
        }
        public int GetY()
        {
            return y;
        }

        public void SetX(int x)
        {
            this.x = x;
        }

        public void SetY(int y)
        {
            this.y = y;
        }

    }
}
