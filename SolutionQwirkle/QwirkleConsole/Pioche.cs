﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class Pioche
    {
        private static List<Pion> pioche;   //on le décrémente à chaque remplissageMain..
        static Random aleatoire = new Random((int)DateTime.Now.Ticks);
        public static void InitialisePioche()
        {
            pioche = new List<Pion>();
            for (int indiceForme = 1; indiceForme <= 6; indiceForme++)
            {
                for (int indiceCouleur = 1; indiceCouleur <=6; indiceCouleur++)
                {
                    for (int indiceCopie = 1; indiceCopie <= 3; indiceCopie++)
                    {
                        pioche.Add(new Pion(indiceForme, indiceCouleur, indiceCopie));
                    }
                }
            }
        }

        public static int GetTaillePioche()
        {
            return pioche.Count;
        }

        public static Pion GetPionPioche()
        {
            if (pioche.Count != 0)
            {
                int piocheCount = pioche.Count - 1;
                int entier = aleatoire.Next(piocheCount);
                Pion retour = pioche[entier];
                pioche.RemoveAt(entier);
                return retour;
            }
            return new Pion(0, 0, 0);
        }

        public static void AddPionPioche(Pion pionAjouter)
        {
            if (pionAjouter.GetId() != 0)
                pioche.Add(pionAjouter);
        }

        public static void ClearPioche()
        {
            pioche.Clear();
        }
    }
}