﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleConsole
{
    public class PlacementPion
    {

        //Variable devant être initialiser a chaque tour
        private static int pion_pose = 0;
        //Variable pour placement 
        private static int xPremierPion, yPremierPion;
        private static int formePremierPion, couleurPremierPion;
        //Boolèen pour les vérifications
        private static bool orientation_pion, type_Pion;
        //OBLIGATOIRE
        private static int xVerifier, yVerifier;
        private static Pion pionVerifier;
        private static int tailleMaxVerticalTerrain, tailleMaxHorizontalTerrain;


        //Code a appelet pour Initialiser chaque changement de tour
        public static void InitialisationPlacementPion()
        {
            pion_pose = 0;
            xPremierPion = 0;
            yPremierPion = 0;
            formePremierPion = 0;
            couleurPremierPion = 0;
        }

        public static void ChangeCoordonnéeXYPremierPion(int coordonneeX, int coordonneeY)
        {
            xPremierPion += coordonneeX;
            yPremierPion += coordonneeY;
        }

        public static bool VerificationPlacement(int x, int y, Pion piecePlacer)
        {
            InitialisationXYPion(x, y, piecePlacer);
            InitialisationVariableTerrain();
            if (VerificationPlacementPremierPionPartie())
                return true;
            if (VerificationPlateauNonVideAutour())
                if (VerificationPlacement())
                    if (VerifNombrePionPlacer())
                        return true;
            return false;
        }

        //Placement Premier Pion Partie

        public static bool VerificationPlacementPremierPionPartie()
        {
            if (Partie.GetTour() == 1 && pion_pose == 0)
            {
                if (xVerifier == Terrain.GetXCentral() && yVerifier == Terrain.GetYCentral())
                {
                    pion_pose = 1;
                    InitialiseSpecificitePion();
                    return true;
                }
            }
            return false;
        }

        //initialisation des variables propres a cette vérification
        public static void InitialisationXYPion(int x, int y, Pion piecePlacer)
        {
            pionVerifier = piecePlacer;
            xVerifier = x;
            yVerifier = y;
        }

        public static void InitialisationVariableTerrain()
        {
            tailleMaxHorizontalTerrain = Terrain.GetNbColonne() - 1;
            tailleMaxVerticalTerrain = Terrain.GetNbLigne() - 1;
        }

        //Verification Code
        public static bool VerificationPlateauNonVideAutour()
        {
            int pionGaucheId, pionDroitId, pionSuperieurId, pionInferieurId;
            pionDroitId = VerificationCoordonneeDroite();
            pionGaucheId = VerificationCoordonneeGauche();
            pionInferieurId = VerificationCoordonneeInferieur();
            pionSuperieurId = VerificationCoordonneeSuperieur();
            if (Terrain.GetPionTerrain(xVerifier, yVerifier).GetId() != 0)
                return false;
            if (pionGaucheId > 0 || pionDroitId > 0 || pionSuperieurId > 0 || pionInferieurId > 0)
                return true;
            return false;
        }
        public static int VerificationCoordonneeGauche()
        {
            if (xVerifier - 1 >= 0)
                return Terrain.GetPionTerrain(xVerifier - 1, yVerifier).GetId();
            else return 0;
        }
        public static int VerificationCoordonneeDroite()
        {
            if (xVerifier + 1 <= tailleMaxHorizontalTerrain)
                return Terrain.GetPionTerrain(xVerifier + 1, yVerifier).GetId();
            else return 0;
        }
        public static int VerificationCoordonneeSuperieur()
        {
            if (yVerifier - 1 >= 0)
                return Terrain.GetPionTerrain(xVerifier, yVerifier - 1).GetId();
            else return 0;
        }
        public static int VerificationCoordonneeInferieur()
        {
            if (yVerifier + 1 <= tailleMaxVerticalTerrain)
                return Terrain.GetPionTerrain(xVerifier, yVerifier + 1).GetId();
            else return 0;
        }

        public static bool VerifNombrePionPlacer()
        {
            switch (pion_pose)
            {
                case 0:
                    return PlacementPremierPion();
                case 1:
                    return PlacementDeuxiemePion();
                default:
                    return PlacementPionSuivantADeux();
            }
        }

        //tour 1 Parametrage
        public static void InitialiseSpecificitePion()
        {
            xPremierPion = xVerifier;
            yPremierPion = yVerifier;
            formePremierPion = pionVerifier.GetForme();
            couleurPremierPion = pionVerifier.GetCouleur();
        }
        //Verification du premier placement de Pion  
        public static bool PlacementPremierPion()
        {
            pion_pose = 1;
            InitialiseSpecificitePion();
            return true;
        }
        //tour 2 Verification
        public static bool Comparaison()
        {
            if (Comparaison_Axe())
                if (Comparaison_Type())
                    return true;
            return false;
        }
        public static bool Comparaison_Axe()
        {
            if (xPremierPion == xVerifier)
                return true;
            if (yPremierPion == yVerifier)
                return true;
            return false;
        }

        public static bool Comparaison_Type()
        {
            if (formePremierPion == pionVerifier.GetForme() || couleurPremierPion == pionVerifier.GetCouleur())
                return true;
            return false;
        }
        public static bool Ligne_Pleine()
        {
            if (VerificationLignePleineVertical())
                return VerificationLignePleineHorizontal();
            return false;
        }
        public static bool VerificationLignePleineVertical()
        {
            if (xVerifier < xPremierPion)
                return LignePleineHorizontal(xVerifier, xPremierPion);
            if (xVerifier > xPremierPion)
                return LignePleineHorizontal(xPremierPion, xVerifier);
            return true;
        }
        public static bool VerificationLignePleineHorizontal()
        {
            if (yVerifier < yPremierPion)
                return LignePleineVertical(yVerifier, yPremierPion);
            if (yVerifier > yPremierPion)
                return LignePleineVertical(yPremierPion, yVerifier);
            return true;
        }
        public static bool LignePleineHorizontal(int PlacementX, int verificationPlacement)
        {
            for (int indice = PlacementX + 1; indice < verificationPlacement; indice++)
                if (Terrain.GetPionTerrain(indice, yPremierPion).GetId() == 0)
                    return false;
            return true;
        }
        public static bool LignePleineVertical(int PlacementY, int verificationPlacement)
        {
            for (int indice = PlacementY + 1; indice < verificationPlacement; indice++)
                if (Terrain.GetPionTerrain(xPremierPion, indice).GetId() == 0)
                    return false;
            return true;
        }


        //Verification du second placement de Pion 
        public static bool PlacementDeuxiemePion()
        {
            if (Comparaison())
                if (Ligne_Pleine())
                {
                    SetParametrePlacement();
                    pion_pose = 2;
                    return true;
                }
            return false;
        }
        //tour 2 Parametrage
        public static void SetParametrePlacement()
        {
            SetAxe();
            SetType();
        }
        public static void SetAxe()
        {
            if (xPremierPion == xVerifier)
                orientation_pion = true;  //alignement horizontal
            else
                orientation_pion = false; //alignement vertical
        }
        public static void SetType()
        {
            int couleurPion = pionVerifier.GetCouleur();
            if (couleurPremierPion == couleurPion)
                type_Pion = true;  //alignement horizontal
            else
                type_Pion = false; //alignement vertical
        }
        //tour 3 Verification
        public static bool VerifParametrePlacement()
        {
            if (VerifAxe())
                if (VerifType())
                    return true;
            return false;
        }
        public static bool VerifType()
        {
            if (formePremierPion == pionVerifier.GetForme() && !type_Pion)
                return true;
            else if (couleurPremierPion == pionVerifier.GetCouleur() && type_Pion)
                return true;
            return false;
        }
        public static bool VerifAxe()
        {
            if (xPremierPion == xVerifier && orientation_pion)
                return true;
            if (yPremierPion == yVerifier && !orientation_pion)
                return true;
            return false;
        }
        //Verification du placement de Pion après le deuxieme placement 
        public static bool PlacementPionSuivantADeux()
        {
            if (VerifParametrePlacement())
                return true;
            return false;
        }
        //Code Appelant la vérification Horizontal et Vertical
        public static bool VerificationPlacement()
        {
            if (VerificationLigneVerticalCompatible())
                if (VerificationLigneHorizontalCompatible())
                    return true;
            return false;
        }
        //Code de Verification Horizontal
        public static bool VerificationLigneHorizontalCompatible()
        {
            List<Pion> listPion = new List<Pion>();
            
            listPion.Add(pionVerifier);
            GetLigneHorizontalDroiteCompatible(listPion);
            GetLigneHorizontalGaucheCompatible(listPion);

            if (Verification_liste_compatible(listPion))
                return Occurence(true);
            return false;
        }
        public static void GetLigneHorizontalDroiteCompatible(List<Pion> listPion)
        {
            Pion VariablePion;
            if (xVerifier + 1 <= tailleMaxHorizontalTerrain)
            {
                VariablePion = Terrain.GetPionTerrain(xVerifier + 1, yVerifier);
                if (VariablePion.GetId() != 0)
                {
                    listPion.Add(VariablePion);
                    if (xVerifier + 2 <= tailleMaxHorizontalTerrain)
                    {
                        VariablePion = Terrain.GetPionTerrain(xVerifier + 2, yVerifier);
                        if (VariablePion.GetId() != 0)
                            listPion.Add(VariablePion);
                    }
                }
            }
        }
        public static void GetLigneHorizontalGaucheCompatible(List<Pion> listPion)
        {
            Pion VariablePion;
            if (xVerifier - 1 >= 0)
            {
                VariablePion = Terrain.GetPionTerrain(xVerifier - 1, yVerifier);
                if (VariablePion.GetId() != 0)
                {
                    listPion.Add(VariablePion);
                    if (xVerifier - 2 >= 0)
                    {
                        VariablePion = Terrain.GetPionTerrain(xVerifier - 2, yVerifier);
                        if (VariablePion.GetId() != 0)
                            listPion.Add(VariablePion);
                    }
                }
            }
        }
        //Code de Verification Vertical
        public static bool VerificationLigneVerticalCompatible()
        {
            List<Pion> listPion = new List<Pion>();
            listPion.Add(pionVerifier);
            VerificationLigneVerticalInferieurCompatible(listPion);
            VerificationLigneVerticalSuperieurCompatible(listPion);

            if (Verification_liste_compatible(listPion))
                return Occurence(false);
            return false;
        }
        public static void VerificationLigneVerticalInferieurCompatible(List<Pion> listPion)
        {
            Pion VariablePion;
            if (yVerifier + 1 <= tailleMaxVerticalTerrain)
            {
                VariablePion = Terrain.GetPionTerrain(xVerifier, yVerifier + 1);
                if (VariablePion.GetId() != 0)
                {
                    listPion.Add(VariablePion);
                    if (yVerifier + 2 <= tailleMaxVerticalTerrain)
                    {
                        VariablePion = Terrain.GetPionTerrain(xVerifier, yVerifier + 2);
                        if (VariablePion.GetId() != 0)
                            listPion.Add(VariablePion);
                    }
                }
            }
        }
        public static void VerificationLigneVerticalSuperieurCompatible(List<Pion> listPion)
        {
            Pion VariablePion;
            if (yVerifier - 1 >= 0)
            {
                VariablePion = Terrain.GetPionTerrain(xVerifier, yVerifier - 1);
                if (VariablePion.GetId() != 0)
                {
                    listPion.Add(VariablePion);
                    if (yVerifier - 2 >= 0)
                    {
                        VariablePion = Terrain.GetPionTerrain(xVerifier, yVerifier - 2);
                        if (VariablePion.GetId() != 0)
                            listPion.Add(VariablePion);
                    }
                }
            }
        }
        //Verification des listes venants des vérifications de lignes
        public static bool Verification_liste_compatible(List<Pion> listPion)
        {
            bool verificationCouleur, verificationForme;
            verificationCouleur = VerificationListeCompatibleCouleur(listPion);
            verificationForme = VerificationListeCompatibleForme(listPion);
            return (verificationCouleur || verificationForme);
        }
        public static bool VerificationListeCompatibleCouleur(List<Pion> listPion)
        {
            int tailleTableau = listPion.Count - 1;
            int valeurPremiereCouleur = listPion[0].GetCouleur();
            while (tailleTableau > 0)
                if (valeurPremiereCouleur != listPion[tailleTableau--].GetCouleur())
                    return false;
            return true;
        }
        public static bool VerificationListeCompatibleForme(List<Pion> listPion)
        {
            int tailleTableau = listPion.Count - 1;
            int valeurPremiereForme = listPion[0].GetForme();
            while (tailleTableau > 0)
                if (valeurPremiereForme != listPion[tailleTableau--].GetForme())
                    return false;
            return true;
        }

        //Verification Complémentaire des Test Ligne 
        public static bool Occurence(bool sens) //booléen : Horizontal=true Vertical=false
        {
            List<int> listId;

            if (sens)
                listId = GetLigneHorizontal();
            else
                listId = GetLigneVertical();

            listId.Add(pionVerifier.GetId());

            return VerifOccurence(listId);
        }
        //Recuperation des occurence dans une ligne
        public static List<int> GetLigneHorizontal()
        {
            List<int> listId = new List<int>();

            if (Terrain.GetPionTerrain(xVerifier, yVerifier).GetId() != 0) //obtention de la case principal si non vide
                listId.Add(Terrain.GetPionTerrain(xVerifier, yVerifier).GetId());

            GetLigneHorizontalDroite(listId);
            GetLigneHorizontalGauche(listId);

            return listId;
        }
        public static void GetLigneHorizontalDroite(List<int> listId)
        {
            int xIncremente = xVerifier + 1;
            if (xIncremente <= tailleMaxHorizontalTerrain)
                while (Terrain.GetPionTerrain(xIncremente, yVerifier).GetId() != 0)
                {
                    listId.Add(Terrain.GetPionTerrain(xIncremente++, yVerifier).GetId());
                    if (xIncremente > tailleMaxHorizontalTerrain)
                        break;
                }
        }
        public static void GetLigneHorizontalGauche(List<int> listId)
        {
            int xDecremente = xVerifier - 1;
            if (xDecremente >= 0)
                while (Terrain.GetPionTerrain(xDecremente, yVerifier).GetId() != 0)
                {
                    listId.Add(Terrain.GetPionTerrain(xDecremente--, yVerifier).GetId());
                    if (xDecremente < 0)
                        break;
                }
        }
        public static List<int> GetLigneVertical()
        {
            List<int> listId = new List<int>();

            if (Terrain.GetPionTerrain(xVerifier, yVerifier).GetId() != 0) //obtention de la case principal si non vide
                listId.Add(Terrain.GetPionTerrain(xVerifier, yVerifier).GetId());

            GetLigneVerticalHaut(listId);
            GetLigneVerticalBas(listId);

            return listId;
        }
        public static void GetLigneVerticalHaut(List<int> listId)
        {
            int yIncremente = yVerifier + 1;

            if (yIncremente <= tailleMaxVerticalTerrain)
                while (Terrain.GetPionTerrain(xVerifier, yIncremente).GetId() != 0)
                {
                    listId.Add(Terrain.GetPionTerrain(xVerifier, yIncremente++).GetId());
                    if (yIncremente > tailleMaxVerticalTerrain)
                        break;
                }
        }
        public static void GetLigneVerticalBas(List<int> listId)
        {
            int yDecremente = yVerifier - 1;
            if (yDecremente >= 0)
                while (Terrain.GetPionTerrain(xVerifier, yDecremente).GetId() != 0)
                {
                    listId.Add(Terrain.GetPionTerrain(xVerifier, yDecremente--).GetId());
                    if (yDecremente < 0)
                        break;
                }
        }
        //Vérification si doublon existant
        public static bool VerifOccurence(List<int> listId)
        {
            int taille = listId.Count - 1; //obtention de la taille de la liste pour la vérification

            for (int i = 0; i < taille; i++)
                for (int j = i + 1; j < taille + 1; j++)
                {
                    int membreGauche = listId[i], membreDroit = listId[j];
                    if ((membreGauche - membreDroit) % 100 == 0) //supprime la possibilité de doublon grace a la difference des id (100-200-300 sont les instances doubles) 
                        return false; //sortie direct pour éviter des itérations inutiles 
                }
            return true;
        }
    }
}
