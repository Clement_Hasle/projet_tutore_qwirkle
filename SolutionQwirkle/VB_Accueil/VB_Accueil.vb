﻿Public Class frm_accueil
    Private Sub frm_accueil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txt_nomj1.MaxLength = 13
        txt_nomj2.MaxLength = 13
        txt_nomj3.MaxLength = 13
        txt_nomj4.MaxLength = 13
        opt_j3start.Visible = False
        opt_j4start.Visible = False
    End Sub
    Private Sub lbl_regles_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lbl_regles.LinkClicked
        System.Diagnostics.Process.Start("https://www.iello.fr/regles/Qwirkle_regles_FR_web.pdf")
    End Sub
    Private Sub txt_nomj1_Click(sender As Object, e As EventArgs) Handles txt_nomj1.Click, txt_nomj2.Click, txt_nomj3.Click, txt_nomj4.Click
        If sender.Text Like "Nom du joueur ?" Then
            sender.Text = ""
        End If
    End Sub
    Private Sub opt_2j_Click(sender As Object, e As EventArgs) Handles opt_2j.Click, opt_3j.Click, opt_4j.Click
        If sender.text Like "2" Then
            txt_nomj3.Enabled = False
            txt_nomj3.Text = "Nom du joueur 3"
            txt_nomj4.Enabled = False
            txt_nomj4.Text = "Nom du joueur 4"
            opt_j3start.Visible = False
            opt_j4start.Visible = False
        End If
        If sender.text Like "3" Then
            txt_nomj3.Enabled = True
            txt_nomj4.Enabled = False
            txt_nomj4.Text = "Nom du joueur 4"
            opt_j3start.Visible = True
            opt_j4start.Visible = False

        End If
        If sender.text Like "4" Then
            txt_nomj3.Enabled = True
            txt_nomj4.Enabled = True
            opt_j3start.Visible = True
            opt_j4start.Visible = True

        End If
        opt_j1start.Checked = True
    End Sub
    Private Sub btn_nouvellePartie_Click(sender As Object, e As EventArgs) Handles btn_nouvellePartie.Click
        Dim formulaireCorrect As Boolean = True
        If txt_nomj1.Text <> txt_nomj2.Text And txt_nomj1.Text <> txt_nomj3.Text And txt_nomj1.Text <> txt_nomj4.Text And txt_nomj2.Text <> txt_nomj3.Text And txt_nomj2.Text <> txt_nomj4.Text And txt_nomj3.Text <> txt_nomj4.Text Then
            If opt_2j.Checked = True Then
                If txt_nomj1.Text Like "Nom du joueur ?" Or txt_nomj1?.Text Like "" Or txt_nomj2.Text Like "Nom du joueur ?" Or txt_nomj2.Text Like "" Then
                    formulaireCorrect = False
                End If
            End If
            If opt_3j.Checked = True Then
                If txt_nomj1.Text Like "Nom du joueur ?" Or txt_nomj2.Text Like "Nom du joueur ?" Or txt_nomj3.Text Like "Nom du joueur ?" Or txt_nomj1.Text Like "" Or txt_nomj2.Text Like "" Or txt_nomj3.Text Like "" Then
                    formulaireCorrect = False
                End If
            End If
            If opt_4j.Checked = True Then
                If txt_nomj1.Text Like "Nom du joueur ?" Or txt_nomj2.Text Like "Nom du joueur ?" Or txt_nomj3.Text Like "Nom du joueur ?" Or txt_nomj4.Text Like "Nom du joueur ?" Or txt_nomj1.Text Like "" Or txt_nomj2.Text Like "" Or txt_nomj3.Text Like "" Or txt_nomj4.Text Like "" Then
                    formulaireCorrect = False
                End If
            End If
            If formulaireCorrect Then
                frm_jeu.Enabled = True
                frm_jeu.ShowDialog()
                Me.Hide()
            Else
                MessageBox.Show("Veuillez saisir le nom des joueurs", "Saisie incomplète")
            End If
        Else
            MessageBox.Show("Veuillez saisir des noms différents pour chaque joueur", "Saisie erronée")
        End If
    End Sub
End Class


