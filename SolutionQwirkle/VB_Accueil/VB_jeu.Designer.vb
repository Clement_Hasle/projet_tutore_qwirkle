﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_jeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_jeu))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lbl_titre = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grp_zoneJ1 = New System.Windows.Forms.GroupBox()
        Me.btn_validerZone1 = New System.Windows.Forms.Button()
        Me.pcb_tuile3zone1 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile5zone1 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile6zone1 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile4zone1 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile2zone1 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile1zone1 = New System.Windows.Forms.PictureBox()
        Me.lbl_score1 = New System.Windows.Forms.Label()
        Me.lbl_nomZone1 = New System.Windows.Forms.Label()
        Me.grp_zoneJ2 = New System.Windows.Forms.GroupBox()
        Me.btn_validerZone2 = New System.Windows.Forms.Button()
        Me.pcb_tuile3zone2 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile5zone2 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile6zone2 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile4zone2 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile2zone2 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile1zone2 = New System.Windows.Forms.PictureBox()
        Me.lbl_score2 = New System.Windows.Forms.Label()
        Me.lbl_nomZone2 = New System.Windows.Forms.Label()
        Me.lbl_textePioche = New System.Windows.Forms.Label()
        Me.lbl_valeurPioche = New System.Windows.Forms.Label()
        Me.btn_home = New System.Windows.Forms.Button()
        Me.btn_retour = New System.Windows.Forms.Button()
        Me.pcb_corbeille = New System.Windows.Forms.PictureBox()
        Me.grid_plateau = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewImageColumn()
        Me.grp_zoneJ3 = New System.Windows.Forms.GroupBox()
        Me.btn_validerZone3 = New System.Windows.Forms.Button()
        Me.pcb_tuile3zone3 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile5zone3 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile6zone3 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile4zone3 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile2zone3 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile1zone3 = New System.Windows.Forms.PictureBox()
        Me.lbl_score3 = New System.Windows.Forms.Label()
        Me.lbl_nomZone3 = New System.Windows.Forms.Label()
        Me.grp_zoneJ4 = New System.Windows.Forms.GroupBox()
        Me.btn_validerZone4 = New System.Windows.Forms.Button()
        Me.pcb_tuile3zone4 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile5zone4 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile6zone4 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile4zone4 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile2zone4 = New System.Windows.Forms.PictureBox()
        Me.pcb_tuile1zone4 = New System.Windows.Forms.PictureBox()
        Me.lbl_score4 = New System.Windows.Forms.Label()
        Me.lbl_nomZone4 = New System.Windows.Forms.Label()
        Me.list_imagesTuiles = New System.Windows.Forms.ImageList(Me.components)
        Me.grp_zoneJ1.SuspendLayout()
        CType(Me.pcb_tuile3zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile5zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile6zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile4zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile2zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile1zone1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_zoneJ2.SuspendLayout()
        CType(Me.pcb_tuile3zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile5zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile6zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile4zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile2zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile1zone2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_corbeille, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grid_plateau, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_zoneJ3.SuspendLayout()
        CType(Me.pcb_tuile3zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile5zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile6zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile4zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile2zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile1zone3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_zoneJ4.SuspendLayout()
        CType(Me.pcb_tuile3zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile5zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile6zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile4zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile2zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcb_tuile1zone4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_titre
        '
        Me.lbl_titre.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_titre.AutoSize = True
        Me.lbl_titre.BackColor = System.Drawing.Color.Pink
        Me.lbl_titre.Font = New System.Drawing.Font("Corbel", 24.0!)
        Me.lbl_titre.ForeColor = System.Drawing.Color.Black
        Me.lbl_titre.Location = New System.Drawing.Point(429, 24)
        Me.lbl_titre.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_titre.Name = "lbl_titre"
        Me.lbl_titre.Padding = New System.Windows.Forms.Padding(5)
        Me.lbl_titre.Size = New System.Drawing.Size(231, 49)
        Me.lbl_titre.TabIndex = 1
        Me.lbl_titre.Text = "Qwirkle : Le jeu"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 5
        '
        'grp_zoneJ1
        '
        Me.grp_zoneJ1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_zoneJ1.BackColor = System.Drawing.Color.Pink
        Me.grp_zoneJ1.Controls.Add(Me.btn_validerZone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile3zone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile5zone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile6zone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile4zone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile2zone1)
        Me.grp_zoneJ1.Controls.Add(Me.pcb_tuile1zone1)
        Me.grp_zoneJ1.Controls.Add(Me.lbl_score1)
        Me.grp_zoneJ1.Controls.Add(Me.lbl_nomZone1)
        Me.grp_zoneJ1.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_zoneJ1.Location = New System.Drawing.Point(33, 108)
        Me.grp_zoneJ1.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ1.Name = "grp_zoneJ1"
        Me.grp_zoneJ1.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ1.Size = New System.Drawing.Size(154, 266)
        Me.grp_zoneJ1.TabIndex = 6
        Me.grp_zoneJ1.TabStop = False
        '
        'btn_validerZone1
        '
        Me.btn_validerZone1.Location = New System.Drawing.Point(38, 217)
        Me.btn_validerZone1.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_validerZone1.Name = "btn_validerZone1"
        Me.btn_validerZone1.Size = New System.Drawing.Size(74, 27)
        Me.btn_validerZone1.TabIndex = 8
        Me.btn_validerZone1.Text = "Valider"
        Me.btn_validerZone1.UseVisualStyleBackColor = True
        '
        'pcb_tuile3zone1
        '
        Me.pcb_tuile3zone1.Location = New System.Drawing.Point(43, 130)
        Me.pcb_tuile3zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile3zone1.Name = "pcb_tuile3zone1"
        Me.pcb_tuile3zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile3zone1.TabIndex = 7
        Me.pcb_tuile3zone1.TabStop = False
        '
        'pcb_tuile5zone1
        '
        Me.pcb_tuile5zone1.Location = New System.Drawing.Point(43, 174)
        Me.pcb_tuile5zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile5zone1.Name = "pcb_tuile5zone1"
        Me.pcb_tuile5zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile5zone1.TabIndex = 6
        Me.pcb_tuile5zone1.TabStop = False
        '
        'pcb_tuile6zone1
        '
        Me.pcb_tuile6zone1.Location = New System.Drawing.Point(91, 174)
        Me.pcb_tuile6zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile6zone1.Name = "pcb_tuile6zone1"
        Me.pcb_tuile6zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile6zone1.TabIndex = 5
        Me.pcb_tuile6zone1.TabStop = False
        '
        'pcb_tuile4zone1
        '
        Me.pcb_tuile4zone1.Location = New System.Drawing.Point(91, 130)
        Me.pcb_tuile4zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile4zone1.Name = "pcb_tuile4zone1"
        Me.pcb_tuile4zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile4zone1.TabIndex = 4
        Me.pcb_tuile4zone1.TabStop = False
        '
        'pcb_tuile2zone1
        '
        Me.pcb_tuile2zone1.Location = New System.Drawing.Point(91, 85)
        Me.pcb_tuile2zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile2zone1.Name = "pcb_tuile2zone1"
        Me.pcb_tuile2zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile2zone1.TabIndex = 3
        Me.pcb_tuile2zone1.TabStop = False
        '
        'pcb_tuile1zone1
        '
        Me.pcb_tuile1zone1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pcb_tuile1zone1.Location = New System.Drawing.Point(43, 85)
        Me.pcb_tuile1zone1.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile1zone1.Name = "pcb_tuile1zone1"
        Me.pcb_tuile1zone1.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile1zone1.TabIndex = 2
        Me.pcb_tuile1zone1.TabStop = False
        '
        'lbl_score1
        '
        Me.lbl_score1.AutoSize = True
        Me.lbl_score1.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_score1.Location = New System.Drawing.Point(40, 47)
        Me.lbl_score1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_score1.Name = "lbl_score1"
        Me.lbl_score1.Size = New System.Drawing.Size(70, 15)
        Me.lbl_score1.TabIndex = 1
        Me.lbl_score1.Text = "Score : 0"
        '
        'lbl_nomZone1
        '
        Me.lbl_nomZone1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nomZone1.Location = New System.Drawing.Point(29, 18)
        Me.lbl_nomZone1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nomZone1.Name = "lbl_nomZone1"
        Me.lbl_nomZone1.Size = New System.Drawing.Size(98, 15)
        Me.lbl_nomZone1.TabIndex = 0
        Me.lbl_nomZone1.Text = "Nom du joueur"
        Me.lbl_nomZone1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grp_zoneJ2
        '
        Me.grp_zoneJ2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_zoneJ2.BackColor = System.Drawing.Color.Pink
        Me.grp_zoneJ2.Controls.Add(Me.btn_validerZone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile3zone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile5zone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile6zone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile4zone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile2zone2)
        Me.grp_zoneJ2.Controls.Add(Me.pcb_tuile1zone2)
        Me.grp_zoneJ2.Controls.Add(Me.lbl_score2)
        Me.grp_zoneJ2.Controls.Add(Me.lbl_nomZone2)
        Me.grp_zoneJ2.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_zoneJ2.Location = New System.Drawing.Point(896, 108)
        Me.grp_zoneJ2.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ2.Name = "grp_zoneJ2"
        Me.grp_zoneJ2.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ2.Size = New System.Drawing.Size(154, 266)
        Me.grp_zoneJ2.TabIndex = 7
        Me.grp_zoneJ2.TabStop = False
        '
        'btn_validerZone2
        '
        Me.btn_validerZone2.Location = New System.Drawing.Point(44, 217)
        Me.btn_validerZone2.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_validerZone2.Name = "btn_validerZone2"
        Me.btn_validerZone2.Size = New System.Drawing.Size(73, 27)
        Me.btn_validerZone2.TabIndex = 8
        Me.btn_validerZone2.Text = "Valider"
        Me.btn_validerZone2.UseVisualStyleBackColor = True
        '
        'pcb_tuile3zone2
        '
        Me.pcb_tuile3zone2.Location = New System.Drawing.Point(47, 124)
        Me.pcb_tuile3zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile3zone2.Name = "pcb_tuile3zone2"
        Me.pcb_tuile3zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile3zone2.TabIndex = 7
        Me.pcb_tuile3zone2.TabStop = False
        '
        'pcb_tuile5zone2
        '
        Me.pcb_tuile5zone2.Location = New System.Drawing.Point(47, 168)
        Me.pcb_tuile5zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile5zone2.Name = "pcb_tuile5zone2"
        Me.pcb_tuile5zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile5zone2.TabIndex = 6
        Me.pcb_tuile5zone2.TabStop = False
        '
        'pcb_tuile6zone2
        '
        Me.pcb_tuile6zone2.Location = New System.Drawing.Point(96, 168)
        Me.pcb_tuile6zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile6zone2.Name = "pcb_tuile6zone2"
        Me.pcb_tuile6zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile6zone2.TabIndex = 5
        Me.pcb_tuile6zone2.TabStop = False
        '
        'pcb_tuile4zone2
        '
        Me.pcb_tuile4zone2.Location = New System.Drawing.Point(96, 124)
        Me.pcb_tuile4zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile4zone2.Name = "pcb_tuile4zone2"
        Me.pcb_tuile4zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile4zone2.TabIndex = 4
        Me.pcb_tuile4zone2.TabStop = False
        '
        'pcb_tuile2zone2
        '
        Me.pcb_tuile2zone2.Location = New System.Drawing.Point(96, 85)
        Me.pcb_tuile2zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile2zone2.Name = "pcb_tuile2zone2"
        Me.pcb_tuile2zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile2zone2.TabIndex = 3
        Me.pcb_tuile2zone2.TabStop = False
        '
        'pcb_tuile1zone2
        '
        Me.pcb_tuile1zone2.Location = New System.Drawing.Point(47, 85)
        Me.pcb_tuile1zone2.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile1zone2.Name = "pcb_tuile1zone2"
        Me.pcb_tuile1zone2.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile1zone2.TabIndex = 2
        Me.pcb_tuile1zone2.TabStop = False
        '
        'lbl_score2
        '
        Me.lbl_score2.AutoSize = True
        Me.lbl_score2.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_score2.Location = New System.Drawing.Point(44, 47)
        Me.lbl_score2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_score2.Name = "lbl_score2"
        Me.lbl_score2.Size = New System.Drawing.Size(70, 15)
        Me.lbl_score2.TabIndex = 1
        Me.lbl_score2.Text = "Score : 0"
        '
        'lbl_nomZone2
        '
        Me.lbl_nomZone2.Location = New System.Drawing.Point(31, 18)
        Me.lbl_nomZone2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nomZone2.Name = "lbl_nomZone2"
        Me.lbl_nomZone2.Size = New System.Drawing.Size(98, 15)
        Me.lbl_nomZone2.TabIndex = 0
        Me.lbl_nomZone2.Text = "Nom du joueur"
        Me.lbl_nomZone2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_textePioche
        '
        Me.lbl_textePioche.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_textePioche.AutoSize = True
        Me.lbl_textePioche.BackColor = System.Drawing.Color.Pink
        Me.lbl_textePioche.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_textePioche.Location = New System.Drawing.Point(431, 689)
        Me.lbl_textePioche.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_textePioche.Name = "lbl_textePioche"
        Me.lbl_textePioche.Padding = New System.Windows.Forms.Padding(5)
        Me.lbl_textePioche.Size = New System.Drawing.Size(198, 28)
        Me.lbl_textePioche.TabIndex = 12
        Me.lbl_textePioche.Text = "Jetons restants : "
        '
        'lbl_valeurPioche
        '
        Me.lbl_valeurPioche.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_valeurPioche.AutoSize = True
        Me.lbl_valeurPioche.BackColor = System.Drawing.Color.Pink
        Me.lbl_valeurPioche.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_valeurPioche.Location = New System.Drawing.Point(629, 689)
        Me.lbl_valeurPioche.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_valeurPioche.Name = "lbl_valeurPioche"
        Me.lbl_valeurPioche.Padding = New System.Windows.Forms.Padding(5)
        Me.lbl_valeurPioche.Size = New System.Drawing.Size(28, 28)
        Me.lbl_valeurPioche.TabIndex = 13
        Me.lbl_valeurPioche.Text = "0"
        '
        'btn_home
        '
        Me.btn_home.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_home.BackColor = System.Drawing.Color.Transparent
        Me.btn_home.BackgroundImage = CType(resources.GetObject("btn_home.BackgroundImage"), System.Drawing.Image)
        Me.btn_home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btn_home.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_home.FlatAppearance.BorderSize = 0
        Me.btn_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_home.Location = New System.Drawing.Point(76, 665)
        Me.btn_home.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(58, 66)
        Me.btn_home.TabIndex = 14
        Me.btn_home.UseVisualStyleBackColor = False
        '
        'btn_retour
        '
        Me.btn_retour.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_retour.BackColor = System.Drawing.Color.Transparent
        Me.btn_retour.BackgroundImage = CType(resources.GetObject("btn_retour.BackgroundImage"), System.Drawing.Image)
        Me.btn_retour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btn_retour.FlatAppearance.BorderSize = 0
        Me.btn_retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_retour.Location = New System.Drawing.Point(703, 681)
        Me.btn_retour.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_retour.Name = "btn_retour"
        Me.btn_retour.Size = New System.Drawing.Size(41, 39)
        Me.btn_retour.TabIndex = 19
        Me.btn_retour.UseVisualStyleBackColor = False
        '
        'pcb_corbeille
        '
        Me.pcb_corbeille.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pcb_corbeille.BackColor = System.Drawing.Color.Transparent
        Me.pcb_corbeille.Image = CType(resources.GetObject("pcb_corbeille.Image"), System.Drawing.Image)
        Me.pcb_corbeille.Location = New System.Drawing.Point(321, 681)
        Me.pcb_corbeille.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_corbeille.Name = "pcb_corbeille"
        Me.pcb_corbeille.Size = New System.Drawing.Size(40, 40)
        Me.pcb_corbeille.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pcb_corbeille.TabIndex = 20
        Me.pcb_corbeille.TabStop = False
        '
        'grid_plateau
        '
        Me.grid_plateau.AllowDrop = True
        Me.grid_plateau.AllowUserToAddRows = False
        Me.grid_plateau.AllowUserToDeleteRows = False
        Me.grid_plateau.AllowUserToResizeColumns = False
        Me.grid_plateau.AllowUserToResizeRows = False
        Me.grid_plateau.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grid_plateau.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.grid_plateau.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        Me.grid_plateau.ColumnHeadersVisible = False
        Me.grid_plateau.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grid_plateau.DefaultCellStyle = DataGridViewCellStyle1
        Me.grid_plateau.Location = New System.Drawing.Point(519, 374)
        Me.grid_plateau.Margin = New System.Windows.Forms.Padding(2)
        Me.grid_plateau.MultiSelect = False
        Me.grid_plateau.Name = "grid_plateau"
        Me.grid_plateau.RowHeadersVisible = False
        Me.grid_plateau.RowHeadersWidth = 30
        Me.grid_plateau.RowTemplate.Height = 24
        Me.grid_plateau.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.grid_plateau.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grid_plateau.Size = New System.Drawing.Size(23, 22)
        Me.grid_plateau.TabIndex = 22
        '
        'Column1
        '
        Me.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Column1.HeaderText = "Column1"
        Me.Column1.MinimumWidth = 6
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 6
        '
        'grp_zoneJ3
        '
        Me.grp_zoneJ3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_zoneJ3.BackColor = System.Drawing.Color.Pink
        Me.grp_zoneJ3.Controls.Add(Me.btn_validerZone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile3zone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile5zone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile6zone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile4zone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile2zone3)
        Me.grp_zoneJ3.Controls.Add(Me.pcb_tuile1zone3)
        Me.grp_zoneJ3.Controls.Add(Me.lbl_score3)
        Me.grp_zoneJ3.Controls.Add(Me.lbl_nomZone3)
        Me.grp_zoneJ3.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_zoneJ3.Location = New System.Drawing.Point(33, 393)
        Me.grp_zoneJ3.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ3.Name = "grp_zoneJ3"
        Me.grp_zoneJ3.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ3.Size = New System.Drawing.Size(154, 266)
        Me.grp_zoneJ3.TabIndex = 10
        Me.grp_zoneJ3.TabStop = False
        '
        'btn_validerZone3
        '
        Me.btn_validerZone3.Location = New System.Drawing.Point(38, 211)
        Me.btn_validerZone3.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_validerZone3.Name = "btn_validerZone3"
        Me.btn_validerZone3.Size = New System.Drawing.Size(72, 27)
        Me.btn_validerZone3.TabIndex = 8
        Me.btn_validerZone3.Text = "Valider"
        Me.btn_validerZone3.UseVisualStyleBackColor = True
        '
        'pcb_tuile3zone3
        '
        Me.pcb_tuile3zone3.Location = New System.Drawing.Point(43, 124)
        Me.pcb_tuile3zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile3zone3.Name = "pcb_tuile3zone3"
        Me.pcb_tuile3zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile3zone3.TabIndex = 7
        Me.pcb_tuile3zone3.TabStop = False
        '
        'pcb_tuile5zone3
        '
        Me.pcb_tuile5zone3.Location = New System.Drawing.Point(43, 168)
        Me.pcb_tuile5zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile5zone3.Name = "pcb_tuile5zone3"
        Me.pcb_tuile5zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile5zone3.TabIndex = 6
        Me.pcb_tuile5zone3.TabStop = False
        '
        'pcb_tuile6zone3
        '
        Me.pcb_tuile6zone3.Location = New System.Drawing.Point(91, 168)
        Me.pcb_tuile6zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile6zone3.Name = "pcb_tuile6zone3"
        Me.pcb_tuile6zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile6zone3.TabIndex = 5
        Me.pcb_tuile6zone3.TabStop = False
        '
        'pcb_tuile4zone3
        '
        Me.pcb_tuile4zone3.Location = New System.Drawing.Point(91, 124)
        Me.pcb_tuile4zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile4zone3.Name = "pcb_tuile4zone3"
        Me.pcb_tuile4zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile4zone3.TabIndex = 4
        Me.pcb_tuile4zone3.TabStop = False
        '
        'pcb_tuile2zone3
        '
        Me.pcb_tuile2zone3.Location = New System.Drawing.Point(91, 85)
        Me.pcb_tuile2zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile2zone3.Name = "pcb_tuile2zone3"
        Me.pcb_tuile2zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile2zone3.TabIndex = 3
        Me.pcb_tuile2zone3.TabStop = False
        '
        'pcb_tuile1zone3
        '
        Me.pcb_tuile1zone3.Location = New System.Drawing.Point(43, 85)
        Me.pcb_tuile1zone3.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile1zone3.Name = "pcb_tuile1zone3"
        Me.pcb_tuile1zone3.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile1zone3.TabIndex = 2
        Me.pcb_tuile1zone3.TabStop = False
        '
        'lbl_score3
        '
        Me.lbl_score3.AutoSize = True
        Me.lbl_score3.Location = New System.Drawing.Point(40, 45)
        Me.lbl_score3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_score3.Name = "lbl_score3"
        Me.lbl_score3.Size = New System.Drawing.Size(70, 15)
        Me.lbl_score3.TabIndex = 1
        Me.lbl_score3.Text = "Score : 0"
        '
        'lbl_nomZone3
        '
        Me.lbl_nomZone3.Location = New System.Drawing.Point(29, 17)
        Me.lbl_nomZone3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nomZone3.Name = "lbl_nomZone3"
        Me.lbl_nomZone3.Size = New System.Drawing.Size(98, 15)
        Me.lbl_nomZone3.TabIndex = 0
        Me.lbl_nomZone3.Text = "Nom du joueur"
        Me.lbl_nomZone3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grp_zoneJ4
        '
        Me.grp_zoneJ4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_zoneJ4.BackColor = System.Drawing.Color.Pink
        Me.grp_zoneJ4.Controls.Add(Me.btn_validerZone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile3zone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile5zone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile6zone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile4zone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile2zone4)
        Me.grp_zoneJ4.Controls.Add(Me.pcb_tuile1zone4)
        Me.grp_zoneJ4.Controls.Add(Me.lbl_score4)
        Me.grp_zoneJ4.Controls.Add(Me.lbl_nomZone4)
        Me.grp_zoneJ4.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_zoneJ4.Location = New System.Drawing.Point(896, 393)
        Me.grp_zoneJ4.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ4.Name = "grp_zoneJ4"
        Me.grp_zoneJ4.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_zoneJ4.Size = New System.Drawing.Size(154, 266)
        Me.grp_zoneJ4.TabIndex = 11
        Me.grp_zoneJ4.TabStop = False
        '
        'btn_validerZone4
        '
        Me.btn_validerZone4.Location = New System.Drawing.Point(43, 211)
        Me.btn_validerZone4.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_validerZone4.Name = "btn_validerZone4"
        Me.btn_validerZone4.Size = New System.Drawing.Size(74, 27)
        Me.btn_validerZone4.TabIndex = 8
        Me.btn_validerZone4.Text = "Valider"
        Me.btn_validerZone4.UseVisualStyleBackColor = True
        '
        'pcb_tuile3zone4
        '
        Me.pcb_tuile3zone4.Location = New System.Drawing.Point(47, 124)
        Me.pcb_tuile3zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile3zone4.Name = "pcb_tuile3zone4"
        Me.pcb_tuile3zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile3zone4.TabIndex = 7
        Me.pcb_tuile3zone4.TabStop = False
        '
        'pcb_tuile5zone4
        '
        Me.pcb_tuile5zone4.Location = New System.Drawing.Point(47, 168)
        Me.pcb_tuile5zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile5zone4.Name = "pcb_tuile5zone4"
        Me.pcb_tuile5zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile5zone4.TabIndex = 6
        Me.pcb_tuile5zone4.TabStop = False
        '
        'pcb_tuile6zone4
        '
        Me.pcb_tuile6zone4.Location = New System.Drawing.Point(96, 168)
        Me.pcb_tuile6zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile6zone4.Name = "pcb_tuile6zone4"
        Me.pcb_tuile6zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile6zone4.TabIndex = 5
        Me.pcb_tuile6zone4.TabStop = False
        '
        'pcb_tuile4zone4
        '
        Me.pcb_tuile4zone4.Location = New System.Drawing.Point(96, 124)
        Me.pcb_tuile4zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile4zone4.Name = "pcb_tuile4zone4"
        Me.pcb_tuile4zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile4zone4.TabIndex = 4
        Me.pcb_tuile4zone4.TabStop = False
        '
        'pcb_tuile2zone4
        '
        Me.pcb_tuile2zone4.Location = New System.Drawing.Point(96, 85)
        Me.pcb_tuile2zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile2zone4.Name = "pcb_tuile2zone4"
        Me.pcb_tuile2zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile2zone4.TabIndex = 3
        Me.pcb_tuile2zone4.TabStop = False
        '
        'pcb_tuile1zone4
        '
        Me.pcb_tuile1zone4.Location = New System.Drawing.Point(47, 85)
        Me.pcb_tuile1zone4.Margin = New System.Windows.Forms.Padding(2)
        Me.pcb_tuile1zone4.Name = "pcb_tuile1zone4"
        Me.pcb_tuile1zone4.Size = New System.Drawing.Size(16, 16)
        Me.pcb_tuile1zone4.TabIndex = 2
        Me.pcb_tuile1zone4.TabStop = False
        '
        'lbl_score4
        '
        Me.lbl_score4.AutoSize = True
        Me.lbl_score4.Location = New System.Drawing.Point(44, 45)
        Me.lbl_score4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_score4.Name = "lbl_score4"
        Me.lbl_score4.Size = New System.Drawing.Size(70, 15)
        Me.lbl_score4.TabIndex = 1
        Me.lbl_score4.Text = "Score : 0"
        '
        'lbl_nomZone4
        '
        Me.lbl_nomZone4.Location = New System.Drawing.Point(31, 17)
        Me.lbl_nomZone4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nomZone4.Name = "lbl_nomZone4"
        Me.lbl_nomZone4.Size = New System.Drawing.Size(98, 15)
        Me.lbl_nomZone4.TabIndex = 0
        Me.lbl_nomZone4.Text = "Nom du joueur"
        Me.lbl_nomZone4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'list_imagesTuiles
        '
        Me.list_imagesTuiles.ImageStream = CType(resources.GetObject("list_imagesTuiles.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.list_imagesTuiles.TransparentColor = System.Drawing.Color.Transparent
        Me.list_imagesTuiles.Images.SetKeyName(0, "CarreBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(1, "CarreJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(2, "CarreOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(3, "CarreRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(4, "CarreVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(5, "CarreViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(6, "CroixBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(7, "CroixJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(8, "CroixOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(9, "CroixRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(10, "CroixVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(11, "CroixViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(12, "EtoileBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(13, "EtoileJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(14, "EtoileOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(15, "EtoileRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(16, "EtoileVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(17, "EtoileViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(18, "LosangeBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(19, "LosangeJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(20, "LosangeOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(21, "LosangeRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(22, "LosangeVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(23, "LosangeViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(24, "RondBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(25, "RondJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(26, "RondOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(27, "RondRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(28, "RondVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(29, "RondViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(30, "TrefleBleu.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(31, "TrefleJaune.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(32, "TrefleOrange.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(33, "TrefleRouge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(34, "TrefleVert.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(35, "TrefleViolet.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(36, "Vierge.jpg")
        Me.list_imagesTuiles.Images.SetKeyName(37, "ViergePremiereCase.jpg")
        '
        'frm_jeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1083, 737)
        Me.Controls.Add(Me.grp_zoneJ4)
        Me.Controls.Add(Me.grp_zoneJ3)
        Me.Controls.Add(Me.grid_plateau)
        Me.Controls.Add(Me.pcb_corbeille)
        Me.Controls.Add(Me.btn_retour)
        Me.Controls.Add(Me.btn_home)
        Me.Controls.Add(Me.lbl_valeurPioche)
        Me.Controls.Add(Me.lbl_textePioche)
        Me.Controls.Add(Me.grp_zoneJ2)
        Me.Controls.Add(Me.grp_zoneJ1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbl_titre)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frm_jeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Jeu Qwirkle"
        Me.grp_zoneJ1.ResumeLayout(False)
        Me.grp_zoneJ1.PerformLayout()
        CType(Me.pcb_tuile3zone1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile5zone1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile6zone1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile4zone1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile2zone1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile1zone1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_zoneJ2.ResumeLayout(False)
        Me.grp_zoneJ2.PerformLayout()
        CType(Me.pcb_tuile3zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile5zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile6zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile4zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile2zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile1zone2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_corbeille, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grid_plateau, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_zoneJ3.ResumeLayout(False)
        Me.grp_zoneJ3.PerformLayout()
        CType(Me.pcb_tuile3zone3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile5zone3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile6zone3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile4zone3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile2zone3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile1zone3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_zoneJ4.ResumeLayout(False)
        Me.grp_zoneJ4.PerformLayout()
        CType(Me.pcb_tuile3zone4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile5zone4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile6zone4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile4zone4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile2zone4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcb_tuile1zone4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_titre As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents grp_zoneJ1 As GroupBox
    Friend WithEvents btn_validerZone1 As Button
    Friend WithEvents pcb_tuile3zone1 As PictureBox
    Friend WithEvents pcb_tuile5zone1 As PictureBox
    Friend WithEvents pcb_tuile6zone1 As PictureBox
    Friend WithEvents pcb_tuile4zone1 As PictureBox
    Friend WithEvents pcb_tuile2zone1 As PictureBox
    Friend WithEvents pcb_tuile1zone1 As PictureBox
    Friend WithEvents lbl_score1 As Label
    Friend WithEvents lbl_nomZone1 As Label
    Friend WithEvents grp_zoneJ2 As GroupBox
    Friend WithEvents btn_validerZone2 As Button
    Friend WithEvents pcb_tuile3zone2 As PictureBox
    Friend WithEvents pcb_tuile5zone2 As PictureBox
    Friend WithEvents pcb_tuile6zone2 As PictureBox
    Friend WithEvents pcb_tuile4zone2 As PictureBox
    Friend WithEvents pcb_tuile2zone2 As PictureBox
    Friend WithEvents pcb_tuile1zone2 As PictureBox
    Friend WithEvents lbl_score2 As Label
    Friend WithEvents lbl_nomZone2 As Label
    Friend WithEvents lbl_textePioche As Label
    Friend WithEvents lbl_valeurPioche As Label
    Friend WithEvents btn_home As Button
    Friend WithEvents btn_retour As Button
    Friend WithEvents pcb_corbeille As PictureBox
    Friend WithEvents grid_plateau As DataGridView
    Friend WithEvents grp_zoneJ3 As GroupBox
    Friend WithEvents btn_validerZone3 As Button
    Friend WithEvents pcb_tuile3zone3 As PictureBox
    Friend WithEvents pcb_tuile5zone3 As PictureBox
    Friend WithEvents pcb_tuile6zone3 As PictureBox
    Friend WithEvents pcb_tuile4zone3 As PictureBox
    Friend WithEvents pcb_tuile2zone3 As PictureBox
    Friend WithEvents pcb_tuile1zone3 As PictureBox
    Friend WithEvents lbl_score3 As Label
    Friend WithEvents lbl_nomZone3 As Label
    Friend WithEvents grp_zoneJ4 As GroupBox
    Friend WithEvents btn_validerZone4 As Button
    Friend WithEvents pcb_tuile3zone4 As PictureBox
    Friend WithEvents pcb_tuile5zone4 As PictureBox
    Friend WithEvents pcb_tuile6zone4 As PictureBox
    Friend WithEvents pcb_tuile4zone4 As PictureBox
    Friend WithEvents pcb_tuile2zone4 As PictureBox
    Friend WithEvents pcb_tuile1zone4 As PictureBox
    Friend WithEvents lbl_score4 As Label
    Friend WithEvents lbl_nomZone4 As Label
    Friend WithEvents list_imagesTuiles As ImageList
    Friend WithEvents Column1 As DataGridViewImageColumn
End Class
