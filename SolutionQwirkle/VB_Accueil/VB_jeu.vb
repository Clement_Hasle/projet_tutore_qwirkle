﻿Public Class frm_jeu
    'Instanciation des joueurs
    Dim j1 As QwirkleConsole.Joueur = New QwirkleConsole.Joueur(1, frm_accueil.txt_nomj1.Text)
    Dim j2 As QwirkleConsole.Joueur = New QwirkleConsole.Joueur(2, frm_accueil.txt_nomj2.Text)
    Dim j3 As QwirkleConsole.Joueur = New QwirkleConsole.Joueur(3, frm_accueil.txt_nomj3.Text)
    Dim j4 As QwirkleConsole.Joueur = New QwirkleConsole.Joueur(4, frm_accueil.txt_nomj4.Text)

    Dim liste_joueur As List(Of QwirkleConsole.Joueur) = New List(Of QwirkleConsole.Joueur)

    Dim nombreJoueur As Integer

    Dim mainInitialeJoueur(5) As QwirkleConsole.Pion 'Variable utilisée pour sauvegarder la main du joueur courant au debut du tour en cas de retour des tuiles posées

    Dim joueurCourant As QwirkleConsole.Joueur

    Dim listePionPlace As New List(Of QwirkleConsole.PionPlace)

    Dim positionPremiereTuileX As Integer = 0
    Dim positionPremiereTuileY As Integer = 0

    'Variables pour gérer la possibilité de placement des pion et l'envoies de ceux ci à la corbeille ou sur le plateau
    Dim tuilePlaceeCeTour As Boolean = False
    Dim tuileJeteeCeTour As Boolean = False
    Dim indiceTuileDeplacee As Integer
    Dim deplacementReussi As Boolean = False
    Dim compteurTuileJete As Integer = 0

    Dim partieTerminee As Boolean = False

    Dim nombreLigne As Integer = 1
    Dim nombreColonne As Integer = 1

    'Largeur et hauteur maximale dataGridView
    Dim widthMaxDataGridView As Integer = 640
    Dim heightMaxDataGridView As Integer = 560

    'Retour de la listes des joueurs pour frm_affichageScore
    Public ReadOnly Property Joueurs() As List(Of QwirkleConsole.Joueur)
        Get
            If frm_accueil.opt_2j.Checked = True Then
                Return New List(Of QwirkleConsole.Joueur) From {j1, j2}
            Else
                If frm_accueil.opt_3j.Checked = True Then
                    Return New List(Of QwirkleConsole.Joueur) From {j1, j2, j3}
                Else
                    Return New List(Of QwirkleConsole.Joueur) From {j1, j2, j3, j4}
                End If
            End If
        End Get
    End Property

    'Procedure daffichage des pions du joueur en fonction de sa main
    Sub AfficherMain(joueur As QwirkleConsole.Joueur)
        Dim formeCouleur As String
        Dim forme_et_couleur_OK As Boolean = True
        Dim mainJoueur As QwirkleConsole.Pion() = joueur.GetMainJoueur()
        For index As Integer = 0 To 5
            Dim entier As Integer = mainJoueur(index).GetForme()
            Select Case mainJoueur(index).GetForme()
                Case 1
                    formeCouleur = "Rond"
                Case 2
                    formeCouleur = "Carre"
                Case 3
                    formeCouleur = "Etoile"
                Case 4
                    formeCouleur = "Losange"
                Case 5
                    formeCouleur = "Trefle"
                Case 6
                    formeCouleur = "Croix"
                Case Else
                    formeCouleur = ""
                    forme_et_couleur_OK = False
            End Select
            Select Case mainJoueur(index).GetCouleur()
                Case 1
                    formeCouleur = formeCouleur & "Rouge.jpg"
                Case 2
                    formeCouleur = formeCouleur & "Orange.jpg"
                Case 3
                    formeCouleur = formeCouleur & "Jaune.jpg"
                Case 4
                    formeCouleur = formeCouleur & "Vert.jpg"
                Case 5
                    formeCouleur = formeCouleur & "Bleu.jpg"
                Case 6
                    formeCouleur = formeCouleur & "Violet.jpg"
                Case Else
                    formeCouleur = ""
                    forme_et_couleur_OK = False
            End Select
            If forme_et_couleur_OK = False Then
                formeCouleur = "Vierge.jpg"
            End If
            Dim grpBox As GroupBox
            Dim imageTuile As PictureBox
            grpBox = Me.Controls("grp_zoneJ" & joueur.GetId.ToString)
            imageTuile = grpBox.Controls("pcb_tuile" & index + 1 & "zone" & joueur.GetId())
            imageTuile.Image = list_imagesTuiles.Images.Item(formeCouleur.ToString)
        Next
    End Sub


    'Procedure qui cache les pions du joueur en affichant des cases vierges
    Sub CacherMain(joueur As QwirkleConsole.Joueur)
        For index As Integer = 1 To 6
            Dim grpBox As GroupBox
            Dim imageTuile As PictureBox
            grpBox = Me.Controls("grp_zoneJ" & joueur.GetId().ToString)
            imageTuile = grpBox.Controls("pcb_tuile" & index & "zone" & joueur.GetId().ToString)
            imageTuile.Image = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
    End Sub

    'Redimensionnement auto du plateau, appel de la fonction de redimensionnement appropriée si nécessaire
    Sub RedimensionnementTerrain()
        Select Case QwirkleConsole.Terrain.VerifPionLimite
            Case 1
                AjoutLigneHaut()
            Case 2
                AjoutColonneDroite()
            Case 3
                AjoutLigneBas()
            Case 4
                AjoutColonneGauche()
            Case 5
                AjoutCarreAutourPremierPion()
        End Select
    End Sub

    'Rajoute 1 ligne en haut du plateau
    Sub AjoutLigneHaut()
        grid_plateau.Rows.Add()
        nombreLigne = nombreLigne + 1
        positionPremiereTuileX = positionPremiereTuileX + 1
        For index1 As Integer = nombreLigne - 1 To 1 Step -1
            For index2 = 0 To nombreColonne - 1
                grid_plateau.Rows(index1).Cells(index2).Value = grid_plateau.Rows(index1 - 1).Cells(index2).Value
            Next
        Next
        For index = 0 To nombreColonne - 1
            grid_plateau.Rows(0).Cells(index).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        For Each pion As QwirkleConsole.PionPlace In listePionPlace
            pion.SetY(pion.GetY + 1)
        Next
        RedimensionneHauteur()
    End Sub

    'Rajoute 1 ligne en bas du plateau
    Sub AjoutLigneBas()
        grid_plateau.Rows.Add()
        nombreLigne = nombreLigne + 1
        For index = 0 To nombreColonne - 1
            grid_plateau.Rows(nombreLigne - 1).Cells(index).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        RedimensionneHauteur()
    End Sub

    'Rajoute 1 colonne a droite du plateau
    Sub AjoutColonneDroite()
        grid_plateau.Columns.Add(New DataGridViewImageColumn())
        nombreColonne = nombreColonne + 1
        grid_plateau.Columns(nombreColonne - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        For index1 = 0 To nombreLigne - 1
            grid_plateau.Rows(index1).Cells(nombreColonne - 1).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        RedimensionneLargeur()
    End Sub

    'Rajoute 1 colonne a gauche du plateau
    Sub AjoutColonneGauche()
        grid_plateau.Columns.Insert(nombreColonne, New DataGridViewImageColumn())
        nombreColonne = nombreColonne + 1
        positionPremiereTuileY = positionPremiereTuileY + 1
        grid_plateau.Columns(nombreColonne - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        For index1 As Integer = 0 To nombreLigne - 1
            For index2 = nombreColonne - 1 To 1 Step -1
                grid_plateau.Rows(index1).Cells(index2).Value = grid_plateau.Rows(index1).Cells(index2 - 1).Value
            Next
        Next
        For index1 = 0 To nombreLigne - 1
            grid_plateau.Rows(index1).Cells(0).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        For Each pion As QwirkleConsole.PionPlace In listePionPlace
            pion.SetX(pion.GetX + 1)
        Next
        RedimensionneLargeur()
    End Sub

    'Rajoute 1 carré autour de la premiere tuile posée au tour 1
    Sub AjoutCarreAutourPremierPion()
        grid_plateau.Columns.Insert(nombreColonne, New DataGridViewImageColumn())
        grid_plateau.Columns.Insert(nombreColonne, New DataGridViewImageColumn())
        grid_plateau.Rows.Add()
        grid_plateau.Rows.Add()
        nombreColonne = nombreColonne + 2
        nombreLigne = nombreLigne + 2
        positionPremiereTuileX = 1
        positionPremiereTuileY = 1
        grid_plateau.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        grid_plateau.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        grid_plateau.Rows(positionPremiereTuileX).Cells(positionPremiereTuileY).Value = grid_plateau.Rows(0).Cells(0).Value
        For index As Integer = 0 To nombreColonne - 1
            grid_plateau.Rows(0).Cells(index).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        For index As Integer = 0 To nombreColonne - 1
            grid_plateau.Rows(2).Cells(index).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        Next
        grid_plateau.Rows(1).Cells(2).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
        grid_plateau.Rows(1).Cells(0).Value = list_imagesTuiles.Images.Item("Vierge.jpg")

        grid_plateau.Height = grid_plateau.Height + 2 * grid_plateau.Rows(0).Height
        grid_plateau.Top = grid_plateau.Location.Y - grid_plateau.Rows(0).Height
        grid_plateau.Width = grid_plateau.Width + 2 * grid_plateau.Columns(0).Width
        grid_plateau.Left = grid_plateau.Location.X - grid_plateau.Columns(0).Width
    End Sub

    'Redimensionne le datagridview en hauteur, sert apres l'ajout de ligne
    Sub RedimensionneHauteur()
        If grid_plateau.Height < heightMaxDataGridView Then
            grid_plateau.Height = grid_plateau.Height + grid_plateau.Rows(0).Height
            grid_plateau.Top = grid_plateau.Location.Y - grid_plateau.Rows(0).Height / 2
        Else
            If grid_plateau.ScrollBars = ScrollBars.Horizontal Then
                grid_plateau.ScrollBars = ScrollBars.Both
            Else
                If grid_plateau.ScrollBars = ScrollBars.None Then
                    grid_plateau.ScrollBars = ScrollBars.Vertical
                End If
            End If
        End If
    End Sub

    'Redimensionne le datagridview en largeur, sert apres l'ajout de colonne
    Sub RedimensionneLargeur()
        If grid_plateau.Width < widthMaxDataGridView Then
            grid_plateau.Width = grid_plateau.Width + grid_plateau.Columns(0).Width
            grid_plateau.Left = grid_plateau.Location.X - grid_plateau.Columns(0).Width / 2
        Else
            If grid_plateau.ScrollBars = ScrollBars.Vertical Then
                grid_plateau.ScrollBars = ScrollBars.Both
            Else
                If grid_plateau.ScrollBars = ScrollBars.None Then
                    grid_plateau.ScrollBars = ScrollBars.Horizontal
                End If
            End If
        End If
    End Sub

    Private Sub VB_jeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        pcb_corbeille.AllowDrop = True

        QwirkleConsole.Comptabilisation.InitTabTour()

        'Initialisation de la pioche
        QwirkleConsole.Pioche.InitialisePioche()

        'Initialisation du terrain
        QwirkleConsole.Terrain.InitTerrain()
        QwirkleConsole.Terrain.InitCentre()

        'Initialisation Tour
        QwirkleConsole.Partie.InitTour()

        'Initialisation Verification Placement
        QwirkleConsole.PlacementPion.InitialisationPlacementPion()

        j1.MiseAJour_Point(0)
        j2.MiseAJour_Point(0)
        j3.MiseAJour_Point(0)
        j4.MiseAJour_Point(0)

        'Remplit la liste de joueur de la partie en fonction du nombre de joueur coché + initialise l'affichage + nombre de joueur total + Initialise les mains des joueurs
        lbl_nomZone1.Text = j1.GetNom
        liste_joueur.Add(j1)
        lbl_nomZone2.Text = j2.GetNom
        liste_joueur.Add(j2)
        QwirkleConsole.MainJoueur.RemplissageMain(j1)
        QwirkleConsole.MainJoueur.RemplissageMain(j2)
        If frm_accueil.opt_2j.Checked = True Then
            grp_zoneJ3.Visible = False
            grp_zoneJ4.Visible = False
            nombreJoueur = 2
        Else
            If frm_accueil.opt_3j.Checked = True Then
                grp_zoneJ4.Visible = False
                lbl_nomZone3.Text = j3.GetNom
                liste_joueur.Add(j3)
                nombreJoueur = 3
                QwirkleConsole.MainJoueur.RemplissageMain(j3)
            Else
                lbl_nomZone3.Text = j3.GetNom
                liste_joueur.Add(j3)
                lbl_nomZone4.Text = j4.GetNom
                liste_joueur.Add(j4)
                nombreJoueur = 4
                QwirkleConsole.MainJoueur.RemplissageMain(j3)
                QwirkleConsole.MainJoueur.RemplissageMain(j4)
            End If
        End If

        'Initialise l'affichage de la pioche
        lbl_valeurPioche.Text = QwirkleConsole.Pioche.GetTaillePioche().ToString

        'Affiche les tuiles du premier joueur et initialise le joueur courant
        If (frm_accueil.opt_j1start.Checked = True) Then
            grp_zoneJ2.Enabled = False
            grp_zoneJ3.Enabled = False
            grp_zoneJ4.Enabled = False
            joueurCourant = j1
        Else
            If (frm_accueil.opt_j2start.Checked = True) Then
                grp_zoneJ1.Enabled = False
                grp_zoneJ3.Enabled = False
                grp_zoneJ4.Enabled = False
                joueurCourant = j2
            Else
                If (frm_accueil.opt_j3start.Checked = True) Then
                    grp_zoneJ2.Enabled = False
                    grp_zoneJ1.Enabled = False
                    grp_zoneJ4.Enabled = False
                    joueurCourant = j3
                Else
                    grp_zoneJ2.Enabled = False
                    grp_zoneJ3.Enabled = False
                    grp_zoneJ1.Enabled = False
                    joueurCourant = j4
                End If
            End If
        End If

        'Cache les pions des joueurs
        CacherMain(j1)
        CacherMain(j2)
        CacherMain(j3)
        CacherMain(j4)

        'Affiche la main du premier joueur & sauvegarde en copie de celle-ci dans mainInitialeJoueur
        AfficherMain(joueurCourant)
        Dim mainJoueur(5) As QwirkleConsole.Pion
        mainJoueur = joueurCourant.GetMainJoueur
        For index As Integer = 0 To 5
            mainInitialeJoueur(index) = mainJoueur(index)
        Next

        'Initialisation du DataGridView
        grid_plateau.Rows.Add()
        grid_plateau.Rows(positionPremiereTuileX).Cells(positionPremiereTuileY).Value = list_imagesTuiles.Images.Item("ViergePremiereCase.jpg")
        grid_plateau.Height = grid_plateau.Rows(0).Height
        grid_plateau.Width = grid_plateau.Columns(0).Width

        'Ajout des évènement MouseMove pour les pictureBox des mains des joueurs
        Dim grpBox As GroupBox
        Dim picBox As PictureBox
        For index1 As Integer = 1 To 4
            grpBox = Me.Controls("grp_zoneJ" & index1)
            For index2 As Integer = 1 To 6
                picBox = grpBox.Controls("pcb_tuile" & index2 & "zone" & index1)
                AddHandler picBox.MouseMove, AddressOf pcb_tuile1zone1_MouseMove
            Next
        Next
    End Sub



    'MouseMove pour le drag'n drop
    Private Sub pcb_tuile1zone1_MouseMove(sender As Object, e As MouseEventArgs) Handles pcb_tuile1zone1.MouseMove
        If e.Button = MouseButtons.Left And sender.Image IsNot Nothing Then
            sender.AllowDrop = False
            Dim rep As DragDropEffects
            Select Case sender.Name
                Case "pcb_tuile1zone1", "pcb_tuile1zone2", "pcb_tuile1zone3", "pcb_tuile1zone4"
                    indiceTuileDeplacee = 0
                Case "pcb_tuile2zone1", "pcb_tuile2zone2", "pcb_tuile2zone3", "pcb_tuile2zone4"
                    indiceTuileDeplacee = 1
                Case "pcb_tuile3zone1", "pcb_tuile3zone2", "pcb_tuile3zone3", "pcb_tuile3zone4"
                    indiceTuileDeplacee = 2
                Case "pcb_tuile4zone1", "pcb_tuile4zone2", "pcb_tuile4zone3", "pcb_tuile4zone4"
                    indiceTuileDeplacee = 3
                Case "pcb_tuile5zone1", "pcb_tuile5zone2", "pcb_tuile5zone3", "pcb_tuile5zone4"
                    indiceTuileDeplacee = 4
                Case "pcb_tuile6zone1", "pcb_tuile6zone2", "pcb_tuile6zone3", "pcb_tuile6zone4"
                    indiceTuileDeplacee = 5
            End Select
            rep = DoDragDrop(sender.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                sender.AllowDrop = True
            End If
            If deplacementReussi Then
                sender.Image = Nothing
                deplacementReussi = False
            End If
        End If
    End Sub

    'DragEnter
    Private Sub pcb_tuile1zone1_DragEnter(sender As Object, e As DragEventArgs) Handles grid_plateau.DragEnter, pcb_corbeille.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    'DragDrop plateau
    Private Sub grid_plateau_DragDrop(sender As Object, e As DragEventArgs) Handles grid_plateau.DragDrop
        Dim mainJoueur(5) As QwirkleConsole.Pion
        mainJoueur = joueurCourant.GetMainJoueur
        If e.Data IsNot Nothing Then
            Dim tuile As Image = e.Data.GetData(DataFormats.Bitmap)
            Dim clientPoint As Point = grid_plateau.PointToClient(New Point(e.X, e.Y))
            Dim info As DataGridView.HitTestInfo = grid_plateau.HitTest(clientPoint.X, clientPoint.Y)
            If info.Type = DataGridViewHitTestType.Cell And tuileJeteeCeTour = False Then
                If QwirkleConsole.PlacementPion.VerificationPlacement(info.ColumnIndex, info.RowIndex, mainJoueur(indiceTuileDeplacee)) Then
                    QwirkleConsole.Terrain.PlacementPionTerrain(mainJoueur(indiceTuileDeplacee), info.ColumnIndex, info.RowIndex)
                    Dim pionPlace As QwirkleConsole.PionPlace = New QwirkleConsole.PionPlace(mainJoueur(indiceTuileDeplacee), info.ColumnIndex, info.RowIndex)
                    listePionPlace.Add(pionPlace)
                    QwirkleConsole.Comptabilisation.MiseAJour_PlacementTour(info.ColumnIndex, info.RowIndex)
                    Me.grid_plateau.Rows(info.RowIndex).Cells(info.ColumnIndex).Value = tuile

                    'Dimensionnement automatique
                    RedimensionnementTerrain()

                    QwirkleConsole.MainJoueur.RetirePionMain(joueurCourant, indiceTuileDeplacee)
                    tuilePlaceeCeTour = True
                    If QwirkleConsole.Partie.GetTour <> 1 Then
                        pcb_corbeille.AllowDrop = False
                    End If
                    deplacementReussi = True
                End If
            End If
        End If
    End Sub

    'DragDrop corbeille
    Private Sub pcb_corbeille_DragDrop(sender As Object, e As DragEventArgs) Handles pcb_corbeille.DragDrop
        Dim mainJoueur(5) As QwirkleConsole.Pion
        mainJoueur = joueurCourant.GetMainJoueur
        If QwirkleConsole.Partie.GetTour <> 1 And QwirkleConsole.Pioche.GetTaillePioche() <> 0 Then
            If tuilePlaceeCeTour = False And compteurTuileJete < QwirkleConsole.Pioche.GetTaillePioche Then
                tuileJeteeCeTour = True
                grid_plateau.AllowDrop = False
                QwirkleConsole.Corbeille.AjoutPionCorbeille(mainJoueur(indiceTuileDeplacee), joueurCourant)
                deplacementReussi = True
                compteurTuileJete = compteurTuileJete + 1
            End If
        Else
            If QwirkleConsole.Partie.GetTour = 1 Then
                MessageBox.Show("Lors du premier tour vous devez placer au moins une tuile", "Premier Tour")
            End If
        End If
    End Sub

    'Actions au bouton retour
    Private Sub btn_retour_Click(sender As Object, e As EventArgs) Handles btn_retour.Click
        Dim x As Integer = 0
        Dim y As Integer = 0
        If tuileJeteeCeTour Then
            tuileJeteeCeTour = False
            grid_plateau.AllowDrop = True
            QwirkleConsole.Corbeille.RetourPionMainJoueur(joueurCourant)
            AfficherMain(joueurCourant)
            compteurTuileJete = 0
        End If
        If tuilePlaceeCeTour Then
            tuilePlaceeCeTour = False
            pcb_corbeille.AllowDrop = True
            For index As Integer = 0 To listePionPlace.Count - 1
                y = listePionPlace(index).GetY
                x = listePionPlace(index).GetX
                QwirkleConsole.Terrain.RetirePionTerrain(x, y)
                grid_plateau.Rows(y).Cells(x).Value = list_imagesTuiles.Images.Item("Vierge.jpg")
            Next
            listePionPlace.Clear()
            joueurCourant.MiseAJour_Main(mainInitialeJoueur)
            'Si le retour est effectué au tour 1, la tuile centrale doit etre marquée
            If QwirkleConsole.Partie.GetTour = 1 Then
                Me.grid_plateau.Rows(positionPremiereTuileX).Cells(positionPremiereTuileY).Value = list_imagesTuiles.Images.Item("ViergePremiereCase.jpg")
            End If
            AfficherMain(joueurCourant)
            QwirkleConsole.PlacementPion.InitialisationPlacementPion()
            QwirkleConsole.Comptabilisation.ResetTourTabTour()
        End If

    End Sub


    'Actions sur l'appuie des bouton de validation des tour = Gestion du tour par tour 
    Private Sub btn_validerZone1_Click(sender As Object, e As EventArgs) Handles btn_validerZone1.Click, btn_validerZone2.Click, btn_validerZone3.Click, btn_validerZone4.Click
        'Lors du tour 1, on change de tour seulement si la joueur a placé au moins une tuile
        If tuilePlaceeCeTour And QwirkleConsole.Partie.GetTour = 1 Or QwirkleConsole.Partie.GetTour <> 1 Then

            'On compte les point et cache la main du joueur
            QwirkleConsole.Comptabilisation.ComptagePoint(joueurCourant)
            Select Case joueurCourant.GetId
                Case 1
                    lbl_score1.Text = "Score : " & joueurCourant.GetPoint
                Case 2
                    lbl_score2.Text = "Score : " & joueurCourant.GetPoint
                Case 3
                    lbl_score3.Text = "Score : " & joueurCourant.GetPoint
                Case 4
                    lbl_score4.Text = "Score : " & joueurCourant.GetPoint
            End Select
            grp_zoneJ1.Enabled = False
            CacherMain(joueurCourant)
            Dim grpBox As GroupBox = Me.Controls("grp_zoneJ" & joueurCourant.GetId)
            grpBox.Enabled = False


            'Verification fin de partie
            If QwirkleConsole.Partie.ConditionFinMain(joueurCourant) And QwirkleConsole.Partie.ConditionFinPioche() Then
                partieTerminee = True
                Me.Close()
            End If

            'Variables pour tester si le joueur peut encore poser des tuiles
            Dim joueurTest As QwirkleConsole.Joueur = New QwirkleConsole.Joueur(16, frm_accueil.txt_nomj1.Text)
            Dim poseImpossible As Boolean = True

            'On remplit sa main
            QwirkleConsole.MainJoueur.RemplissageMain(joueurCourant)

            'test pour savoir si le joueur peut encore poser des tuiles
            For index As Integer = 0 To liste_joueur.Count - 1
                joueurTest = liste_joueur(index)
                If QwirkleConsole.Partie.PlacementPossible(joueurTest) = False And QwirkleConsole.Partie.ConditionFinPioche() Then
                Else
                    poseImpossible = False
                End If
            Next

            If poseImpossible Then
                partieTerminee = True
                Me.Close()
            End If

            'On choisit le joueur suivant
            If nombreJoueur >= joueurCourant.GetId + 1 Then
                joueurCourant = liste_joueur(joueurCourant.GetId)
                grpBox = Me.Controls("grp_zoneJ" & joueurCourant.GetId)
                grpBox.Enabled = True
            Else
                joueurCourant = j1
                grpBox = Me.Controls("grp_zoneJ" & joueurCourant.GetId)
                grpBox.Enabled = True
            End If

            'Messagebox pour permettre le changement de tour sans que les joueurs voient les pions des autres/ Puis on affiche la main du nouveau joueur courant
            If partieTerminee = False Then
                MessageBox.Show("Changement de joueur" & vbCrLf & vbCrLf & "Au tour de " & joueurCourant.GetNom.ToString, "Changement")
            End If

            AfficherMain(joueurCourant)

            'reinitialisation de la fonction de verification
            QwirkleConsole.PlacementPion.InitialisationPlacementPion()

            'reinitialisation de la corbeille et envoie des tuiles jetées dans la pioche
            QwirkleConsole.Corbeille.EnvoiePionPioche()
            QwirkleConsole.Corbeille.resetCorbeille()
            tuilePlaceeCeTour = False
            tuileJeteeCeTour = False

            'Incrementation du tour
            QwirkleConsole.Partie.IncrementeTour()

            pcb_corbeille.AllowDrop = True
            grid_plateau.AllowDrop = True

            'On met a jour le label pioche
            lbl_valeurPioche.Text = QwirkleConsole.Pioche.GetTaillePioche().ToString

            Dim mainJoueur(5) As QwirkleConsole.Pion
            mainJoueur = joueurCourant.GetMainJoueur
            For index As Integer = 0 To 5
                mainInitialeJoueur(index) = mainJoueur(index)
            Next

            listePionPlace.Clear()

            compteurTuileJete = 0
        Else
            MessageBox.Show("Lors du premier tour vous devez placer au moins une tuile", "Premier Tour")
        End If

    End Sub

    'Pour empecher l'utilisateur de selectionner une case, car production de résidus graphique lors du redimensionnement
    Private Sub grid_plateau_SelectionChanged(sender As Object, e As EventArgs) Handles grid_plateau.SelectionChanged
        grid_plateau.ClearSelection()
    End Sub

    'Actions lors de la fermeture du form / bouton Home, affiche une messageBox si le joueur veut quitter la partie sinon affiche le score
    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Me.Close()
    End Sub
    Private Sub frm_jeu_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If partieTerminee = False Then
            If MessageBox.Show("Voulez-vous vraiment quitter la partie ?", "Fermeture", MessageBoxButtons.YesNo) = DialogResult.No Then
                e.Cancel = True
            Else
                frm_affichageScore.Show()
            End If
        Else
            frm_affichageScore.Show()
        End If
    End Sub

End Class
