﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_affichageScore
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_affichageScore))
        Me.lbl_textGagnant = New System.Windows.Forms.Label()
        Me.lbl_nomGagnant = New System.Windows.Forms.Label()
        Me.lbl_text2eme = New System.Windows.Forms.Label()
        Me.lbl_text4eme = New System.Windows.Forms.Label()
        Me.lbl_text3eme = New System.Windows.Forms.Label()
        Me.lbl_nom2eme = New System.Windows.Forms.Label()
        Me.lbl_nom3eme = New System.Windows.Forms.Label()
        Me.lbl_nom4eme = New System.Windows.Forms.Label()
        Me.btn_confirmScore = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_textGagnant
        '
        Me.lbl_textGagnant.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_textGagnant.AutoSize = True
        Me.lbl_textGagnant.BackColor = System.Drawing.Color.Pink
        Me.lbl_textGagnant.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_textGagnant.Location = New System.Drawing.Point(53, 26)
        Me.lbl_textGagnant.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_textGagnant.Name = "lbl_textGagnant"
        Me.lbl_textGagnant.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_textGagnant.Size = New System.Drawing.Size(314, 24)
        Me.lbl_textGagnant.TabIndex = 0
        Me.lbl_textGagnant.Text = "Le vainqueur de la partie est "
        '
        'lbl_nomGagnant
        '
        Me.lbl_nomGagnant.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nomGagnant.BackColor = System.Drawing.Color.Pink
        Me.lbl_nomGagnant.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nomGagnant.Location = New System.Drawing.Point(53, 50)
        Me.lbl_nomGagnant.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nomGagnant.Name = "lbl_nomGagnant"
        Me.lbl_nomGagnant.Size = New System.Drawing.Size(314, 21)
        Me.lbl_nomGagnant.TabIndex = 1
        Me.lbl_nomGagnant.Text = "Nomdupremier avec xx points"
        Me.lbl_nomGagnant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_text2eme
        '
        Me.lbl_text2eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_text2eme.AutoSize = True
        Me.lbl_text2eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_text2eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_text2eme.Location = New System.Drawing.Point(46, 102)
        Me.lbl_text2eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_text2eme.Name = "lbl_text2eme"
        Me.lbl_text2eme.Size = New System.Drawing.Size(77, 15)
        Me.lbl_text2eme.TabIndex = 2
        Me.lbl_text2eme.Text = "Deuxième :"
        '
        'lbl_text4eme
        '
        Me.lbl_text4eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_text4eme.AutoSize = True
        Me.lbl_text4eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_text4eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_text4eme.Location = New System.Drawing.Point(36, 156)
        Me.lbl_text4eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_text4eme.Name = "lbl_text4eme"
        Me.lbl_text4eme.Size = New System.Drawing.Size(84, 15)
        Me.lbl_text4eme.TabIndex = 3
        Me.lbl_text4eme.Text = "Quatrième :"
        '
        'lbl_text3eme
        '
        Me.lbl_text3eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_text3eme.AutoSize = True
        Me.lbl_text3eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_text3eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_text3eme.Location = New System.Drawing.Point(39, 130)
        Me.lbl_text3eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_text3eme.Name = "lbl_text3eme"
        Me.lbl_text3eme.Size = New System.Drawing.Size(84, 15)
        Me.lbl_text3eme.TabIndex = 4
        Me.lbl_text3eme.Text = "Troisième :"
        '
        'lbl_nom2eme
        '
        Me.lbl_nom2eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nom2eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_nom2eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom2eme.Location = New System.Drawing.Point(139, 102)
        Me.lbl_nom2eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nom2eme.Name = "lbl_nom2eme"
        Me.lbl_nom2eme.Size = New System.Drawing.Size(234, 17)
        Me.lbl_nom2eme.TabIndex = 5
        Me.lbl_nom2eme.Text = "Nomdudeuxieme avec xx points"
        Me.lbl_nom2eme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_nom3eme
        '
        Me.lbl_nom3eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nom3eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_nom3eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom3eme.Location = New System.Drawing.Point(139, 130)
        Me.lbl_nom3eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nom3eme.Name = "lbl_nom3eme"
        Me.lbl_nom3eme.Size = New System.Drawing.Size(234, 17)
        Me.lbl_nom3eme.TabIndex = 6
        Me.lbl_nom3eme.Text = "Nomdutroisieme avec xx points"
        Me.lbl_nom3eme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_nom4eme
        '
        Me.lbl_nom4eme.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nom4eme.BackColor = System.Drawing.Color.Pink
        Me.lbl_nom4eme.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nom4eme.Location = New System.Drawing.Point(139, 155)
        Me.lbl_nom4eme.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nom4eme.Name = "lbl_nom4eme"
        Me.lbl_nom4eme.Size = New System.Drawing.Size(234, 17)
        Me.lbl_nom4eme.TabIndex = 7
        Me.lbl_nom4eme.Text = "Nomduquatrieme avec xx points"
        Me.lbl_nom4eme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_confirmScore
        '
        Me.btn_confirmScore.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_confirmScore.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_confirmScore.Location = New System.Drawing.Point(133, 202)
        Me.btn_confirmScore.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_confirmScore.Name = "btn_confirmScore"
        Me.btn_confirmScore.Size = New System.Drawing.Size(145, 31)
        Me.btn_confirmScore.TabIndex = 8
        Me.btn_confirmScore.Text = "Retourner au menu"
        Me.btn_confirmScore.UseVisualStyleBackColor = True
        '
        'frm_affichageScore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(417, 254)
        Me.Controls.Add(Me.btn_confirmScore)
        Me.Controls.Add(Me.lbl_nom4eme)
        Me.Controls.Add(Me.lbl_nom3eme)
        Me.Controls.Add(Me.lbl_nom2eme)
        Me.Controls.Add(Me.lbl_text3eme)
        Me.Controls.Add(Me.lbl_text4eme)
        Me.Controls.Add(Me.lbl_text2eme)
        Me.Controls.Add(Me.lbl_nomGagnant)
        Me.Controls.Add(Me.lbl_textGagnant)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frm_affichageScore"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Score"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_textGagnant As Label
    Friend WithEvents lbl_nomGagnant As Label
    Friend WithEvents lbl_text2eme As Label
    Friend WithEvents lbl_text4eme As Label
    Friend WithEvents lbl_text3eme As Label
    Friend WithEvents lbl_nom2eme As Label
    Friend WithEvents lbl_nom3eme As Label
    Friend WithEvents lbl_nom4eme As Label
    Friend WithEvents btn_confirmScore As Button
End Class
