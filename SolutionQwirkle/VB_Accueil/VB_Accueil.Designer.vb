﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_accueil))
        Me.lbl_titre = New System.Windows.Forms.Label()
        Me.lbl_regles = New System.Windows.Forms.LinkLabel()
        Me.btn_nouvellePartie = New System.Windows.Forms.Button()
        Me.grp_Jstart = New System.Windows.Forms.GroupBox()
        Me.opt_j4start = New System.Windows.Forms.RadioButton()
        Me.opt_j3start = New System.Windows.Forms.RadioButton()
        Me.opt_j2start = New System.Windows.Forms.RadioButton()
        Me.opt_j1start = New System.Windows.Forms.RadioButton()
        Me.grp_nomsJ = New System.Windows.Forms.GroupBox()
        Me.txt_nomj4 = New System.Windows.Forms.TextBox()
        Me.txt_nomj3 = New System.Windows.Forms.TextBox()
        Me.txt_nomj2 = New System.Windows.Forms.TextBox()
        Me.txt_nomj1 = New System.Windows.Forms.TextBox()
        Me.lbl_nbJoueur = New System.Windows.Forms.Label()
        Me.opt_2j = New System.Windows.Forms.RadioButton()
        Me.opt_3j = New System.Windows.Forms.RadioButton()
        Me.opt_4j = New System.Windows.Forms.RadioButton()
        Me.grp_Jstart.SuspendLayout()
        Me.grp_nomsJ.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_titre
        '
        Me.lbl_titre.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_titre.AutoSize = True
        Me.lbl_titre.BackColor = System.Drawing.Color.Pink
        Me.lbl_titre.Font = New System.Drawing.Font("Corbel", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_titre.ForeColor = System.Drawing.Color.Black
        Me.lbl_titre.Location = New System.Drawing.Point(253, 24)
        Me.lbl_titre.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_titre.Name = "lbl_titre"
        Me.lbl_titre.Padding = New System.Windows.Forms.Padding(5)
        Me.lbl_titre.Size = New System.Drawing.Size(231, 49)
        Me.lbl_titre.TabIndex = 0
        Me.lbl_titre.Text = "Qwirkle : Le jeu"
        '
        'lbl_regles
        '
        Me.lbl_regles.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_regles.AutoSize = True
        Me.lbl_regles.LinkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbl_regles.Location = New System.Drawing.Point(330, 412)
        Me.lbl_regles.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_regles.Name = "lbl_regles"
        Me.lbl_regles.Size = New System.Drawing.Size(72, 13)
        Me.lbl_regles.TabIndex = 3
        Me.lbl_regles.TabStop = True
        Me.lbl_regles.Text = "Règles du jeu"
        '
        'btn_nouvellePartie
        '
        Me.btn_nouvellePartie.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btn_nouvellePartie.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btn_nouvellePartie.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nouvellePartie.Location = New System.Drawing.Point(303, 352)
        Me.btn_nouvellePartie.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_nouvellePartie.Name = "btn_nouvellePartie"
        Me.btn_nouvellePartie.Size = New System.Drawing.Size(133, 37)
        Me.btn_nouvellePartie.TabIndex = 4
        Me.btn_nouvellePartie.Text = "Nouvelle Partie"
        Me.btn_nouvellePartie.UseVisualStyleBackColor = False
        '
        'grp_Jstart
        '
        Me.grp_Jstart.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_Jstart.BackColor = System.Drawing.Color.Pink
        Me.grp_Jstart.Controls.Add(Me.opt_j4start)
        Me.grp_Jstart.Controls.Add(Me.opt_j3start)
        Me.grp_Jstart.Controls.Add(Me.opt_j2start)
        Me.grp_Jstart.Controls.Add(Me.opt_j1start)
        Me.grp_Jstart.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_Jstart.ForeColor = System.Drawing.Color.Black
        Me.grp_Jstart.Location = New System.Drawing.Point(396, 155)
        Me.grp_Jstart.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_Jstart.Name = "grp_Jstart"
        Me.grp_Jstart.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_Jstart.Size = New System.Drawing.Size(180, 171)
        Me.grp_Jstart.TabIndex = 5
        Me.grp_Jstart.TabStop = False
        Me.grp_Jstart.Text = "Qui commence ?"
        '
        'opt_j4start
        '
        Me.opt_j4start.AutoSize = True
        Me.opt_j4start.Location = New System.Drawing.Point(47, 130)
        Me.opt_j4start.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_j4start.Name = "opt_j4start"
        Me.opt_j4start.Size = New System.Drawing.Size(81, 19)
        Me.opt_j4start.TabIndex = 3
        Me.opt_j4start.Text = "Joueur 4"
        Me.opt_j4start.UseVisualStyleBackColor = True
        '
        'opt_j3start
        '
        Me.opt_j3start.AutoSize = True
        Me.opt_j3start.Location = New System.Drawing.Point(47, 98)
        Me.opt_j3start.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_j3start.Name = "opt_j3start"
        Me.opt_j3start.Size = New System.Drawing.Size(81, 19)
        Me.opt_j3start.TabIndex = 2
        Me.opt_j3start.Text = "Joueur 3"
        Me.opt_j3start.UseVisualStyleBackColor = True
        '
        'opt_j2start
        '
        Me.opt_j2start.AutoSize = True
        Me.opt_j2start.Location = New System.Drawing.Point(47, 68)
        Me.opt_j2start.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_j2start.Name = "opt_j2start"
        Me.opt_j2start.Size = New System.Drawing.Size(81, 19)
        Me.opt_j2start.TabIndex = 1
        Me.opt_j2start.Text = "Joueur 2"
        Me.opt_j2start.UseVisualStyleBackColor = True
        '
        'opt_j1start
        '
        Me.opt_j1start.AutoSize = True
        Me.opt_j1start.BackColor = System.Drawing.Color.Pink
        Me.opt_j1start.Checked = True
        Me.opt_j1start.Location = New System.Drawing.Point(47, 38)
        Me.opt_j1start.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_j1start.Name = "opt_j1start"
        Me.opt_j1start.Size = New System.Drawing.Size(81, 19)
        Me.opt_j1start.TabIndex = 0
        Me.opt_j1start.TabStop = True
        Me.opt_j1start.Text = "Joueur 1"
        Me.opt_j1start.UseVisualStyleBackColor = False
        '
        'grp_nomsJ
        '
        Me.grp_nomsJ.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grp_nomsJ.BackColor = System.Drawing.Color.Pink
        Me.grp_nomsJ.Controls.Add(Me.txt_nomj4)
        Me.grp_nomsJ.Controls.Add(Me.txt_nomj3)
        Me.grp_nomsJ.Controls.Add(Me.txt_nomj2)
        Me.grp_nomsJ.Controls.Add(Me.txt_nomj1)
        Me.grp_nomsJ.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_nomsJ.ForeColor = System.Drawing.Color.Black
        Me.grp_nomsJ.Location = New System.Drawing.Point(167, 157)
        Me.grp_nomsJ.Margin = New System.Windows.Forms.Padding(2)
        Me.grp_nomsJ.Name = "grp_nomsJ"
        Me.grp_nomsJ.Padding = New System.Windows.Forms.Padding(2)
        Me.grp_nomsJ.Size = New System.Drawing.Size(180, 169)
        Me.grp_nomsJ.TabIndex = 6
        Me.grp_nomsJ.TabStop = False
        Me.grp_nomsJ.Text = "Noms des joueurs"
        '
        'txt_nomj4
        '
        Me.txt_nomj4.Enabled = False
        Me.txt_nomj4.Location = New System.Drawing.Point(32, 127)
        Me.txt_nomj4.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_nomj4.Name = "txt_nomj4"
        Me.txt_nomj4.Size = New System.Drawing.Size(116, 21)
        Me.txt_nomj4.TabIndex = 3
        Me.txt_nomj4.Text = "Nom du joueur 4"
        '
        'txt_nomj3
        '
        Me.txt_nomj3.Enabled = False
        Me.txt_nomj3.Location = New System.Drawing.Point(32, 95)
        Me.txt_nomj3.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_nomj3.Name = "txt_nomj3"
        Me.txt_nomj3.Size = New System.Drawing.Size(116, 21)
        Me.txt_nomj3.TabIndex = 2
        Me.txt_nomj3.Text = "Nom du joueur 3"
        '
        'txt_nomj2
        '
        Me.txt_nomj2.Location = New System.Drawing.Point(32, 65)
        Me.txt_nomj2.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_nomj2.Name = "txt_nomj2"
        Me.txt_nomj2.Size = New System.Drawing.Size(116, 21)
        Me.txt_nomj2.TabIndex = 1
        Me.txt_nomj2.Text = "Nom du joueur 2"
        '
        'txt_nomj1
        '
        Me.txt_nomj1.Location = New System.Drawing.Point(32, 33)
        Me.txt_nomj1.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_nomj1.Name = "txt_nomj1"
        Me.txt_nomj1.Size = New System.Drawing.Size(116, 21)
        Me.txt_nomj1.TabIndex = 0
        Me.txt_nomj1.Text = "Nom du joueur 1"
        '
        'lbl_nbJoueur
        '
        Me.lbl_nbJoueur.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_nbJoueur.AutoSize = True
        Me.lbl_nbJoueur.BackColor = System.Drawing.Color.Pink
        Me.lbl_nbJoueur.Font = New System.Drawing.Font("Corbel", 9.0!)
        Me.lbl_nbJoueur.ForeColor = System.Drawing.Color.Black
        Me.lbl_nbJoueur.Location = New System.Drawing.Point(226, 106)
        Me.lbl_nbJoueur.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nbJoueur.Name = "lbl_nbJoueur"
        Me.lbl_nbJoueur.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_nbJoueur.Size = New System.Drawing.Size(110, 20)
        Me.lbl_nbJoueur.TabIndex = 7
        Me.lbl_nbJoueur.Text = "Nombre de joueurs :"
        '
        'opt_2j
        '
        Me.opt_2j.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.opt_2j.Appearance = System.Windows.Forms.Appearance.Button
        Me.opt_2j.AutoSize = True
        Me.opt_2j.BackColor = System.Drawing.Color.WhiteSmoke
        Me.opt_2j.Checked = True
        Me.opt_2j.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt_2j.Location = New System.Drawing.Point(377, 106)
        Me.opt_2j.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_2j.Name = "opt_2j"
        Me.opt_2j.Padding = New System.Windows.Forms.Padding(6, 0, 4, 0)
        Me.opt_2j.Size = New System.Drawing.Size(34, 25)
        Me.opt_2j.TabIndex = 8
        Me.opt_2j.TabStop = True
        Me.opt_2j.Text = "2"
        Me.opt_2j.UseVisualStyleBackColor = False
        '
        'opt_3j
        '
        Me.opt_3j.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.opt_3j.Appearance = System.Windows.Forms.Appearance.Button
        Me.opt_3j.AutoSize = True
        Me.opt_3j.BackColor = System.Drawing.Color.WhiteSmoke
        Me.opt_3j.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt_3j.Location = New System.Drawing.Point(430, 106)
        Me.opt_3j.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_3j.Name = "opt_3j"
        Me.opt_3j.Padding = New System.Windows.Forms.Padding(6, 0, 4, 0)
        Me.opt_3j.Size = New System.Drawing.Size(34, 25)
        Me.opt_3j.TabIndex = 9
        Me.opt_3j.TabStop = True
        Me.opt_3j.Text = "3"
        Me.opt_3j.UseVisualStyleBackColor = False
        '
        'opt_4j
        '
        Me.opt_4j.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.opt_4j.Appearance = System.Windows.Forms.Appearance.Button
        Me.opt_4j.AutoSize = True
        Me.opt_4j.BackColor = System.Drawing.Color.WhiteSmoke
        Me.opt_4j.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.opt_4j.Location = New System.Drawing.Point(479, 106)
        Me.opt_4j.Margin = New System.Windows.Forms.Padding(2)
        Me.opt_4j.Name = "opt_4j"
        Me.opt_4j.Padding = New System.Windows.Forms.Padding(6, 0, 4, 0)
        Me.opt_4j.Size = New System.Drawing.Size(34, 25)
        Me.opt_4j.TabIndex = 10
        Me.opt_4j.TabStop = True
        Me.opt_4j.Text = "4"
        Me.opt_4j.UseVisualStyleBackColor = False
        '
        'frm_accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(728, 447)
        Me.Controls.Add(Me.opt_4j)
        Me.Controls.Add(Me.opt_3j)
        Me.Controls.Add(Me.opt_2j)
        Me.Controls.Add(Me.lbl_nbJoueur)
        Me.Controls.Add(Me.grp_nomsJ)
        Me.Controls.Add(Me.grp_Jstart)
        Me.Controls.Add(Me.btn_nouvellePartie)
        Me.Controls.Add(Me.lbl_regles)
        Me.Controls.Add(Me.lbl_titre)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frm_accueil"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accueil Qwirkle"
        Me.grp_Jstart.ResumeLayout(False)
        Me.grp_Jstart.PerformLayout()
        Me.grp_nomsJ.ResumeLayout(False)
        Me.grp_nomsJ.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_titre As Label
    Friend WithEvents lbl_regles As LinkLabel
    Friend WithEvents btn_nouvellePartie As Button
    Friend WithEvents grp_Jstart As GroupBox
    Friend WithEvents grp_nomsJ As GroupBox
    Friend WithEvents lbl_nbJoueur As Label
    Friend WithEvents opt_2j As RadioButton
    Friend WithEvents opt_3j As RadioButton
    Friend WithEvents opt_4j As RadioButton
    Friend WithEvents opt_j4start As RadioButton
    Friend WithEvents opt_j3start As RadioButton
    Friend WithEvents opt_j2start As RadioButton
    Friend WithEvents opt_j1start As RadioButton
    Friend WithEvents txt_nomj4 As TextBox
    Friend WithEvents txt_nomj3 As TextBox
    Friend WithEvents txt_nomj2 As TextBox
    Friend WithEvents txt_nomj1 As TextBox
End Class
