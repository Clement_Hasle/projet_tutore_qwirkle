﻿Public Class frm_affichageScore
    Private Sub btn_confirmScore_Click(sender As Object, e As EventArgs) Handles btn_confirmScore.Click
        Me.Close()
    End Sub

    Public Sub frm_affichageScore_FormShow(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim liste_joueur As List(Of QwirkleConsole.Joueur) = frm_jeu.Joueurs
        liste_joueur = QwirkleConsole.Partie.FinPartie(liste_joueur)
        'On affiche le nom et le score des 2 premiers joueurs
        lbl_nomGagnant.Text = liste_joueur.ElementAt(0).GetNom & " avec " & liste_joueur.ElementAt(0).GetPoint() & " points"
        lbl_nom2eme.Text = liste_joueur.ElementAt(1).GetNom & " avec " & liste_joueur.ElementAt(1).GetPoint() & " points"
        lbl_nom3eme.Hide() 'On cache les joueurs 4 et 3 qui seront réactivés selon le nombre de joueur
        lbl_text3eme.Hide()
        lbl_nom4eme.Hide()
        lbl_text4eme.Hide()
        'On vérifie s'il est nécessaire d'afficher les joueurs 3 et 4 en fonction du nombre de joueurs, puis on les affiche le cas échéant 
        If frm_accueil.opt_3j.Checked = True Then
            lbl_nom3eme.Text = liste_joueur.ElementAt(2).GetNom & " avec " & liste_joueur.ElementAt(2).GetPoint() & " points"
            lbl_nom3eme.Show()
            lbl_text3eme.Show()
        ElseIf frm_accueil.opt_4j.Checked = True Then
            lbl_nom3eme.Text = liste_joueur.ElementAt(2).GetNom & " avec " & liste_joueur.ElementAt(2).GetPoint() & " points"
            lbl_nom4eme.Text = liste_joueur.ElementAt(3).GetNom & " avec " & liste_joueur.ElementAt(3).GetPoint() & " points"
            lbl_nom3eme.Show()
            lbl_text3eme.Show()
            lbl_nom4eme.Show()
            lbl_text4eme.Show()
        End If
        frm_jeu.Close()
    End Sub

    Private Sub frm_affichageScore_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        frm_accueil.Show()
        Dim txtBox As TextBox
        For index As Integer = 1 To 4
            txtBox = frm_accueil.grp_nomsJ.Controls("txt_nomj" & index)
            txtBox.Text = "Nom du joueur " & index.ToString
        Next
    End Sub
End Class